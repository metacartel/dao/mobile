/**
 * @format
 * @flow
 */

import "babel-polyfill";
import "./shim.js";

import { AppRegistry, YellowBox } from "react-native";
import { name as appName } from "./app.json";
import serviceMessaging from "./serviceMessaging";
import App from "./src/App";

try {
  YelloBox.ignoreWarnings(["Require cycle"]);
} catch (err) {
  // HACK: Do nothing.
}

AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerHeadlessTask(
  "RNFirebaseBackgroundMessage",
  () => serviceMessaging
);
