# MetaCartel DAO

[MetaCartel DAO GraphQL Endpoint](https://thegraph.com/explorer/subgraph/jamesyoung/metacarteldao)
[Raw Transaction Endpoint](https://us-central1-meta-dao.cloudfunctions.net/receiveSignedTransaction)
[Submit Proposal Details Endpoint](https://us-central1-meta-dao.cloudfunctions.net/submitProposalDetails)
[Get Proposal Details Endpoint](https://us-central1-meta-dao.cloudfunctions.net/getProposalDetails)
[Create Proposal Trigger Endpoint](https://us-central1-meta-dao.cloudfunctions.net/createProposalTrigger)

## Development

Pre-requisites:

* React Native
* Node
*

* Install modules `node_modules` using `npm install`
* Run nodeify with custom modules `npm run hack`
* Run `npm run reset` to reset metro bundler cache
* Run `npm start:bundler:ios` to create jsbundle file for ios
* On terminal `react-native run-ios`

## Note

For every new npm package installed, `npm run hack` must be run. Currently there is an npm way to add it on `postinstall` hook but I haven't tested it yet.
