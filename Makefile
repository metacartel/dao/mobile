default: run

.PHONY: run
run:
	@echo "Running on connected device..."
	@react-native run-android

.PHONY: dev
dev:
	@echo "Running development server..."
	@react-native start -- --reset-cache

.PHONY: start
start:
	@echo "Starting metro server..."
	@npm start

.PHONY: key.google
key.google:
	@keytool -list -v -alias androiddebugkey -keystore ~/.android/debug.keystore

.PHONY: key.facebook
key.facebook:
	@keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64
