// @flow

import { applyMiddleware, createStore, compose } from "redux";
import { createLogger } from "redux-logger";
import { createReduxSdkMiddleware } from "@archanova/sdk";
import createSagaMiddleware from "redux-saga";
import createRootReducer from "./reducers";
import configureSdk from "./helpers/sdk";
import rootSaga from "./sagas";

const sagaMiddleware = createSagaMiddleware();

const sdk = configureSdk();
const sdkMiddleware = createReduxSdkMiddleware(sdk);

const getMiddleware = () => {
  return applyMiddleware(sdkMiddleware, sagaMiddleware, createLogger());
};

export const store: Store = configureStore();

export default function configureStore(preloadedState?: any) {
  const composeEnhancer =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(
    createRootReducer(),
    preloadedState,
    composeEnhancer(getMiddleware())
  );

  sagaMiddleware.run(rootSaga);
  return store;
}
