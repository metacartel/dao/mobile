import * as actionTypes from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case actionTypes.GET_EXCHANGE_RATES_SUCCESS:
      return {
        ...state,
        usdExchangeRate: action.payload.usdRate,
      };
    case actionTypes.HOME_LOADED:
      return {
        ...state,
        confirmedSignedOut: false,
      };
    case actionTypes.SIGNOUT:
      return {
        ...state,
        confirmedSignedOut: true,
      };
    case actionTypes.HOME_UNLOADED:
      return {};
    default:
      return state;
  }
};
