import {combineReducers} from 'redux';
import {reduxSdkReducer} from '@archanova/sdk';

import auth from './auth';
import home from './home';
import members from './members';
import proposals from './proposals';

export default () => combineReducers({
  auth,
  home,
  members,
  proposals,
  sdk: reduxSdkReducer,
});
