import * as actionTypes from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case actionTypes.CARTEL_CALL_LIST_SUCCESS:
      return {
        ...state,
        proposals: action.payload.data.proposals,
      };
    case actionTypes.PROPOSALS_LOADED:
      return {
        ...state,
      };
    case actionTypes.PROPOSALS_UNLOADED:
      return {};
    default:
      return state;
  }
};
