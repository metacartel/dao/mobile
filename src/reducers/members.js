import * as actionTypes from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case actionTypes.CARTEL_CALL_LIST_SUCCESS:
      return {
        ...state,
        members: action.payload.data.members,
      };
    case actionTypes.MEMBERS_LOADED:
      return {
        ...state,
      };
    case actionTypes.MEMBERS_UNLOADED:
      return {};
    default:
      return state;
  }
};
