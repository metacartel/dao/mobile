/**
 * MetaCartel DAO
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { View } from "react-native";
import { Provider } from "react-redux";
import { useScreens } from "react-native-screens";
import firebase from "react-native-firebase";
import AsyncStorage from "@react-native-community/async-storage";
import RootContainer from "./navigators/RootContainer";
import { store } from "./store";

useScreens();

export default class App extends Component {
  async componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners();
  }

  componentWillUnmount() {
    this.unsubscribeNotificationListener();
    this.unsubscribeNotificationOpenedListener();
  }

  componentDidUpdate(prevProps) {}

  async checkPermission() {
    try {
      const enabled = await firebase.messaging().hasPermission();
      if (enabled) {
        this.getToken();
      } else {
        this.requestPermission();
      }
      await firebase.messaging().subscribeToTopic("/topics/proposals");
    } catch (error) {
      console.log("#####", error);
    }
  }

  async getToken() {
    try {
      let fcmToken = await AsyncStorage.getItem("fcmToken");
      if (!fcmToken) {
        fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
          await AsyncStorage.setItem("fcmToken", fcmToken);
        }
      }
    } catch (error) {
      console.log("#####", error);
    }
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      this.getToken();
    } catch (error) {
      console.log("Notification permission rejected!");
    }
  }

  async createNotificationListeners() {
    this.unsubscribeNotificationListener = firebase
      .notifications()
      .onNotification(notification => {
        const { title, body } = notification;
        console.log("### notif", title, body);
      });

    this.unsubscribeNotificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const { title, body } = notificationOpen;
        console.log("### notif", title, body);
      });

    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      console.log("### notif", title, body);
    }

    this.unsubscribeMessageListener = firebase
      .messaging()
      .onMessage(message => {
        console.log(JSON.stringify(message));
      });
  }

  render() {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    );
  }
}
