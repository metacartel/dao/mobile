// @flow

import abi from 'ethjs-abi';

const contractCode = () => {
  return '0xF6a5F3FfAc0a7Cc040732FDA1e0E51795Cf3D0f6';
};

const contractAbi = [
  // 0
  {
    "constant": false,
    "inputs": [
      {
        "name": "proposalIndex",
        "type": "uint256"
      }
    ],
    "name": "abort",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  // 1
  {
    "constant": false,
    "inputs": [
      {
        "name": "proposalIndex",
        "type": "uint256"
      }
    ],
    "name": "processProposal",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  // 2
  {
    "constant": false,
    "inputs": [
      {
        "name": "sharesToBurn",
        "type": "uint256"
      }
    ],
    "name": "ragequit",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  // 3
  {
    "constant": false,
    "inputs": [
      {
        "name": "applicant",
        "type": "address"
      },
      {
        "name": "tokenTribute",
        "type": "uint256"
      },
      {
        "name": "sharesRequested",
        "type": "uint256"
      },
      {
        "name": "details",
        "type": "string"
      }
    ],
    "name": "submitProposal",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  // 4
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "name": "proposalIndex",
        "type": "uint256"
      },
      {
        "indexed": true,
        "name": "delegateKey",
        "type": "address"
      },
      {
        "indexed": true,
        "name": "memberAddress",
        "type": "address"
      },
      {
        "indexed": true,
        "name": "applicant",
        "type": "address"
      },
      {
        "indexed": false,
        "name": "tokenTribute",
        "type": "uint256"
      },
      {
        "indexed": false,
        "name": "sharesRequested",
        "type": "uint256"
      }
    ],
    "name": "SubmitProposal",
    "type": "event"
  },
  // 5
  {
    "constant": false,
    "inputs": [
      {
        "name": "proposalIndex",
        "type": "uint256"
      },
      {
        "name": "uintVote",
        "type": "uint8"
      }
    ],
    "name": "submitVote",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  // 6
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "name": "proposalIndex",
        "type": "uint256"
      },
      {
        "indexed": true,
        "name": "delegateKey",
        "type": "address"
      },
      {
        "indexed": true,
        "name": "memberAddress",
        "type": "address"
      },
      {
        "indexed": false,
        "name": "uintVote",
        "type": "uint8"
      }
    ],
    "name": "SubmitVote",
    "type": "event"
  },
  // 7
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "name": "proposalIndex",
        "type": "uint256"
      },
      {
        "indexed": true,
        "name": "applicant",
        "type": "address"
      },
      {
        "indexed": true,
        "name": "memberAddress",
        "type": "address"
      },
      {
        "indexed": false,
        "name": "tokenTribute",
        "type": "uint256"
      },
      {
        "indexed": false,
        "name": "sharesRequested",
        "type": "uint256"
      },
      {
        "indexed": false,
        "name": "didPass",
        "type": "bool"
      }
    ],
    "name": "ProcessProposal",
    "type": "event"
  },
  // 8
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "name": "memberAddress",
        "type": "address"
      },
      {
        "indexed": false,
        "name": "sharesToBurn",
        "type": "uint256"
      }
    ],
    "name": "Ragequit",
    "type": "event"
  },
  // 9
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "name": "proposalIndex",
        "type": "uint256"
      },
      {
        "indexed": false,
        "name": "applicantAddress",
        "type": "address"
      }
    ],
    "name": "Abort",
    "type": "event"
  },
  // 10
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "name": "memberAddress",
        "type": "address"
      },
      {
        "indexed": false,
        "name": "newDelegateKey",
        "type": "address"
      }
    ],
    "name": "UpdateDelegateKey",
    "type": "event"
  },
  // 11
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "name": "summoner",
        "type": "address"
      },
      {
        "indexed": false,
        "name": "shares",
        "type": "uint256"
      }
    ],
    "name": "SummonComplete",
    "type": "event"
  },
  // 12
  {
    "inputs": [
      {
        "name": "summoner",
        "type": "address"
      },
      {
        "name": "_approvedToken",
        "type": "address"
      },
      {
        "name": "_periodDuration",
        "type": "uint256"
      },
      {
        "name": "_votingPeriodLength",
        "type": "uint256"
      },
      {
        "name": "_gracePeriodLength",
        "type": "uint256"
      },
      {
        "name": "_abortWindow",
        "type": "uint256"
      },
      {
        "name": "_proposalDeposit",
        "type": "uint256"
      },
      {
        "name": "_dilutionBound",
        "type": "uint256"
      },
      {
        "name": "_processingReward",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  // 13
  {
    "constant": false,
    "inputs": [
      {
        "name": "newDelegateKey",
        "type": "address"
      }
    ],
    "name": "updateDelegateKey",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  // 14
  {
    "constant": true,
    "inputs": [],
    "name": "abortWindow",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 15
  {
    "constant": true,
    "inputs": [],
    "name": "approvedToken",
    "outputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 16
  {
    "constant": true,
    "inputs": [
      {
        "name": "highestIndexYesVote",
        "type": "uint256"
      }
    ],
    "name": "canRagequit",
    "outputs": [
      {
        "name": "",
        "type": "bool"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 17
  {
    "constant": true,
    "inputs": [],
    "name": "dilutionBound",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 18
  {
    "constant": true,
    "inputs": [],
    "name": "getCurrentPeriod",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 19
  {
    "constant": true,
    "inputs": [
      {
        "name": "memberAddress",
        "type": "address"
      },
      {
        "name": "proposalIndex",
        "type": "uint256"
      }
    ],
    "name": "getMemberProposalVote",
    "outputs": [
      {
        "name": "",
        "type": "uint8"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 20
  {
    "constant": true,
    "inputs": [],
    "name": "getProposalQueueLength",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 21
  {
    "constant": true,
    "inputs": [],
    "name": "gracePeriodLength",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 22
  {
    "constant": true,
    "inputs": [],
    "name": "guildBank",
    "outputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 23
  {
    "constant": true,
    "inputs": [
      {
        "name": "startingPeriod",
        "type": "uint256"
      }
    ],
    "name": "hasVotingPeriodExpired",
    "outputs": [
      {
        "name": "",
        "type": "bool"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 24
  {
    "constant": true,
    "inputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "name": "memberAddressByDelegateKey",
    "outputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 25
  {
    "constant": true,
    "inputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "name": "members",
    "outputs": [
      {
        "name": "delegateKey",
        "type": "address"
      },
      {
        "name": "shares",
        "type": "uint256"
      },
      {
        "name": "exists",
        "type": "bool"
      },
      {
        "name": "highestIndexYesVote",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 26
  {
    "constant": true,
    "inputs": [],
    "name": "periodDuration",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 27
  {
    "constant": true,
    "inputs": [],
    "name": "processingReward",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 28
  {
    "constant": true,
    "inputs": [],
    "name": "proposalDeposit",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 29
  {
    "constant": true,
    "inputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "name": "proposalQueue",
    "outputs": [
      {
        "name": "proposer",
        "type": "address"
      },
      {
        "name": "applicant",
        "type": "address"
      },
      {
        "name": "sharesRequested",
        "type": "uint256"
      },
      {
        "name": "startingPeriod",
        "type": "uint256"
      },
      {
        "name": "yesVotes",
        "type": "uint256"
      },
      {
        "name": "noVotes",
        "type": "uint256"
      },
      {
        "name": "processed",
        "type": "bool"
      },
      {
        "name": "didPass",
        "type": "bool"
      },
      {
        "name": "aborted",
        "type": "bool"
      },
      {
        "name": "tokenTribute",
        "type": "uint256"
      },
      {
        "name": "details",
        "type": "string"
      },
      {
        "name": "maxTotalSharesAtYesVote",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 30
  {
    "constant": true,
    "inputs": [],
    "name": "summoningTime",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 31
  {
    "constant": true,
    "inputs": [],
    "name": "totalShares",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 32
  {
    "constant": true,
    "inputs": [],
    "name": "totalSharesRequested",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  // 33
  {
    "constant": true,
    "inputs": [],
    "name": "votingPeriodLength",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  }
];

const abortByteCode = (params) => {
  return abi.encodeMethod(contractAbi[0], [...params]);
};

const processProposalByteCode = (params) => {
  return abi.encodeMethod(contractAbi[1], [...params]);
};

const rageQuitByteCode = (params) => {
  return abi.encodeMethod(contractAbi[2], [...params]);
};

const submitProposalByteCode = (params) => {
  return abi.encodeMethod(contractAbi[3], [...params]);
};

const submitVoteByteCode = (params) => {
  return abi.encodeMethod(contractAbi[6], [...params]);
};

const updateDelegateKeyByteCode = (params) => {
  return abi.encodeMethod(contractAbi[13], [...params]);
};

const getCurrentPeriodByteCode = (params) => {
  return abi.encodeMethod(contractAbi[18], [...params]);
};

const getMemberProposalVoteByteCode = (params) => {
  return abi.encodeMethod(contractAbi[19], [...params]);
};

module.exports = {
  abortByteCode,
  processProposalByteCode,
  rageQuitByteCode,
  submitProposalByteCode,
  submitVoteByteCode,
  updateDelegateKeyByteCode,
  getCurrentPeriodByteCode,
  getMemberProposalVoteByteCode,
};
