// @flow

import {LoginManager, AccessToken} from 'react-native-fbsdk';
import {GoogleSignin, statusCode} from 'react-native-google-signin';
import firebase from 'react-native-firebase';

const bootGoogle = async () => {
  await GoogleSignin.configure({
    webClientId: '1039157717483-69e1bjfcv18a33e60058q4j7fu9u96ih.apps.googleusercontent.com',
    offlineAccess: true,
  });
};

const loginWithGoogle = async () => {
  await GoogleSignin.hasPlayServices();
  const {idToken, accessToken} = await GoogleSignin.signIn();
  const credential = firebase.auth.GoogleAuthProvider.credential(idToken, accessToken);
  return userCredential = await firebase.auth().signInWithCredential(credential);
};

const loginWithFacebook = async () => {
  const result = await LoginManager.logInWithReadPermissions(['email']);
  if (result.isCancelled) {
    throw new Error('User cancelled request');
  }

  const data = await AccessToken.getCurrentAccessToken();
  if (!data) {
    throw new Error('Something went wrong obtaining the user access token');
  }

  const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
  return await firebase.auth().signInWithCredential(credential);
};

const loginWithTwitter = async () => {
  // TODO: implement
};

module.exports = {
  bootGoogle,
  loginWithGoogle,
  loginWithFacebook,
  loginWithTwitter,
};
