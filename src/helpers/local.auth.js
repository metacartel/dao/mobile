// @flow

import firebase from 'react-native-firebase';

const createAccount = async (email, password) => {
  return await firebase.auth().createUserWithEmailAndPassword(email, password);
};

const signInLocal = async (email, password) => {
  return await firebase.auth().signInWithEmailAndPassword(email, password);
};

const signOut = async () => {
  return await firebase.auth().signOut();
};

const authStateChanged = async (cb) => {
  return await firebase.auth().onAuthStateChanged(cb);
};

module.exports = {
  createAccount,
  signInLocal,
  signOut,
  authStateChanged,
};
