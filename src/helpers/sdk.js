// @flow

import {Sdk, SdkEnvironmentNames, getSdkEnvironment} from '@archanova/sdk';
import AsyncStorage from '@react-native-community/async-storage';
import crypto from 'crypto';

const sdkEnv = getSdkEnvironment(SdkEnvironmentNames.Rinkeby);
export const sdk = new Sdk(
  sdkEnv
    .setConfig('storageAdapter', AsyncStorage)
);

export default () => {
  const wrapper = async () => {
    await sdk.initialize();

    const {accountAddress} = sdk.state;

    if (!accountAddress) {
      const {items} = await sdk.getConnectedAccounts();

      if (items.length) {
        const [{address}] = items;
        await sdk.connectAccount(address);
      } else {
        const label = crypto.randomBytes(12).toString('hex');
        await sdk.createAccount(label);
      }
    }
  };
  wrapper();

  return sdk;
}

export const searchAccount = async (options) => {
  return await sdk.searchAccount(options);
};

export const estimateAccountDeployment = async () => {
  return await sdk.estimateAccountDeployment();
};

export const deployAccount = async () => {
  await sdk.estimateAccountDeployment();
  return await sdk.deployAccount();
};

export const sendAccountTransaction = async (recepient, value, data) => {
  const estimated = await sdk.estimateAccountTransaction(recepient, value, data);
  return await sdk.submitAccountTransaction(estimated);
};

export const signPersonalMessage = async (message) => {
  return await sdk.signPersonalMessage(message);
};

export const reset = async () => {
  await sdk.reset();
};
