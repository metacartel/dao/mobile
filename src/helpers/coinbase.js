const getExchangeRates = async () => {
  return fetch('https://api.coinbase.com/v2/exchange-rates?currency=ETH');
};

module.exports = {
  getExchangeRates,
};
