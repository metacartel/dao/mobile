import {ApolloClient} from 'apollo-client';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {createHttpLink} from 'apollo-link-http';
import gql from 'graphql-tag';

const cache = new InMemoryCache();
const link = createHttpLink({
  uri: 'https://api.thegraph.com/subgraphs/name/ffimnsr/metacartel_dao',
});

const client = new ApolloClient({
  cache,
  link,
});

const getListOfApplicants = async () => {
  return await client.query({
    query: gql`
      {
        applicants(first: 10) {
          id
          timestamp
          proposalIndex
          memberAddress
          applicantAddress
          tokenTribute
          sharesRequested
          didPass
          aborted
          proposal {
            yesVotes
            noVotes
            processed
          }
        }
      }
      `
  });
};

const getApplicant = async (id) => {
  return await client.query({
    query: gql`
      query Applicant($id: String!) {
        applicant(id: $id) {
          id
          timestamp
          proposalIndex
          memberAddress
          applicantAddress
          tokenTribute
          sharesRequested
          didPass
          aborted
          proposal {
            yesVotes
            noVotes
            processed
          }
        }
      }
      `
  });
};


const getListOfMembers = async () => {
  return await client.query({
    query: gql`
      {
        members(first: 10) {
          id
          delegateKey
          shares
          isActive
          tokenTribute
          didRagequit
          votes {
            proposal {
              proposalIndex
              timestamp
              tokenTribute
              sharesRequested
              processed
            }
          }
        }
      }
      `
  });
};

const getMember = async (id) => {
  return await client.query({
    query: gql`
      query Member($id: String!) {
        member(id: $id) {
          id
          delegateKey
          shares
          isActive
          tokenTribute
          didRagequit
          votes {
            proposal {
              proposalIndex
              yesVotes
              noVotes
            }
          }
          submissions {
            proposalIndex
            yesVotes
            noVotes
            processed
            didPass
            aborted
          }
        }
      }
      `
  });
};

const getListOfProposals = async () => {
  return await client.query({
    query: gql`
      {
        proposals(first: 10) {
          timestamp
          proposalIndex
          startingPeriod
          applicantAddress
          tokenTribute
          sharesRequested
          yesVotes
          noVotes
          didPass
          aborted
          details
          votes {
            memberAddress
            uintVote
          }
        }
      }
      `
  });
};

const getProposal = async (id) => {
  return await client.query({
    query: gql`
      query Proposal($id: String!) {
        proposal(id: $id) {
          id
          timestamp
          proposalIndex
          startingPeriod
          applicantAddress
          yesVotes
          noVotes
          didPass
          aborted
          details
          votes {
            memberAddress
            uintVote
          }
        }
      }
      `
  });
};

const getListOfVotes = async () => {
  return await client.query({
    query: gql`
      {
        votes(first: 10) {
          id
          timestamp
          proposalIndex
          uintVote
          proposal {
            timestamp
            startingPeriod
            processed
            didPass
            aborted
          }
          memberAddress
        }
      }
      `
  });
};

const getVote = async (id) => {
  return await client.query({
    query: gql`
      query Vote($id: String!) {
        votes(id: $id) {
          id
          timestamp
          proposalIndex
          uintVote
          proposal {
            timestamp
            startingPeriod
            processed
            didPass
            aborted
          }
          memberAddress
        }
      }
      `
  });
};

module.exports = {
  getListOfApplicants,
  getApplicant,
  getListOfMembers,
  getMember,
  getListOfProposals,
  getProposal,
  getListOfVotes,
  getVote,
};
