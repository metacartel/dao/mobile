import firebase from 'react-native-firebase';

const registerArchanovaDetails = async (currentUser, data) => {
  return await firebase.firestore().collection('users').doc(currentUser.uid).set(data);
};

const getArchanovaDetails = async (currentUser, data) => {
  return await firebase.firestore().collection('users').doc(currentUser.uid).get();
};

module.exports = {
  registerArchanovaDetails,
  getArchanovaDetails,
};
