// Onboarding
const STARSTRUCK = require('./images/star-struck.png');

// SignUp
const PSEUDONYM = require('./images/pseudonym.png');
const EMAIL = require('./images/email.png');
const PASSWORD = require('./images/password.png');

//Main
const PROPOSAL = require('./images/proposal.png');
const HOME = require('./images/home.png');
const MEMBER = require('./images/member.png');
const DROPDOWN = require('./images/dropdown.png');

//Proposal
const PLUS = require('./images/plus.png');
const CLOCK = require('./images/clock.png');
const THROWUP = require('./images/throw-up.png');

const METACARTEL = require('./images/metacartel.png');
const GRAPH = require('./images/graph.png');
const STAR = require('./images/star.png');
const RIGHT = require('./images/right.png');
const DOWN = require('./images/down.png');
const UP = require('./images/up.png');

export default {
    CLOCK,
    DOWN,
    DROPDOWN,
    EMAIL,
    GRAPH,
    HOME,
    MEMBER,
    METACARTEL,
    PASSWORD,
    PLUS,
    PROPOSAL,
    RIGHT,
    PSEUDONYM,
    STAR,
    STARSTRUCK,
    THROWUP,
    UP
};
