import React, { Component } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import SplashScreen from "react-native-splash-screen";
import { Button } from "native-base";

import Images from "../../../assets";

export default class SignUpEmail extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state } = navigation;
    return {
      title: "Sign up with Email",
      headerStyle: {
        backgroundColor: "#fff",
        elevation: 0,
        marginTop: 20,
        borderBottomWidth: 1,
        borderColor: "#000",
      },
      headerTitleStyle: styles.headerTitle,
      headerRight: <View></View>,
    };
  };

  componentDidMount() {}

  render() {
    return (
      <View style={styles.container}>
        <Text>SignUP</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 60,
    paddingRight: 60,
    alignItems: "center",
  },
  headerTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    lineHeight: 23,
    textAlign: "center",
    color: "#4C4C4C",
  },
});
