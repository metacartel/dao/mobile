import React, { Component } from "react";
import { View, Image, ScrollView, StyleSheet, Text } from "react-native";
import { Button } from "native-base";
import { connect } from "react-redux";
import Switch from "react-native-switch-pro";
import Images from "../../../assets";
import * as actionTypes from "../../../constants/actionTypes";

const mapStateToProps = state => ({ ...state.home, sdk: state.sdk });

const mapDispatchToProps = dispatch => ({
  onTogglePushNotifications: () =>
    dispatch({ type: actionTypes.TOGGLE_PUSH_NOTIFICATIONS }),
  onToggleEmailNotifications: () =>
    dispatch({ type: actionTypes.TOGGLE_EMAIL_NOTIFICATIONS }),
  onToggleWeeklyDigest: () =>
    dispatch({ type: actionTypes.TOGGLE_WEEKLY_DIGEST }),
  onRageQuit: () => dispatch({ type: actionTypes.RAGEQUIT }),
  onSignOut: () => dispatch({ type: actionTypes.SIGNOUT }),
});

class MemberSettings extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Member Settings",
      headerStyle: {
        backgroundColor: "#fff",
        elevation: 0,
        textAlign: "center",
        borderBottomWidth: 1,
        borderColor: "#000",
      },
      headerTitleStyle: styles.headerTitle,
      headerRight: <View />,
    };
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.confirmedSignedOUt !== prevProps.confirmedSignedOut &&
      this.props.confirmedSignedOut === true
    ) {
      this.props.navigation.navigate("LoginNavigator");
    }
  }

  rageQuit = () => this.props.onRageQuit();
  signOut = () => this.props.onSignOut();

  render() {
    const { account } = this.props.sdk;
    const accountAddress =
      account && account.address ? account.address : "undefined";
    const accountAddressSpliced =
      accountAddress.slice(0, 7) +
      "..." +
      accountAddress.slice(-4, accountAddress.length);

    return (
      <ScrollView
        contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
      >
        <View style={styles.container}>
          <View style={styles.delegateKeyDiv}>
            <View>
              <Text style={styles.name}>Delegate Key</Text>
              <Text style={styles.details}>{accountAddressSpliced}</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text style={[styles.details, { marginRight: 10 }]}>Edit</Text>
              <Image
                style={styles.editIcon}
                source={Images.RIGHT}
                resizeMode="contain"
              />
            </View>
          </View>

          <Button
            transparent
            onPress={this.rageQuit}
            style={{ marginBottom: 90 }}
          >
            <Text style={[styles.name, { marginTop: 19, marginBottom: 19 }]}>
              Ragequit
            </Text>
          </Button>
          <Button
            transparent
            onPress={this.signOut}
            style={{ marginBottom: 90 }}
          >
            <Text style={[styles.name]}>Sign Out</Text>
          </Button>
          <Text style={styles.settingsText}>Notification Settings</Text>
          <View
            style={[
              styles.listItem,
              { borderTopWidth: 1, borderTopColor: "rgba(0, 0, 0, 0.15)" },
            ]}
          >
            <Text style={styles.name}>Instant Push Notifications</Text>
            <Switch
              onSyncPress={value => {
                console.log(value);
              }}
              value={false}
              width={84}
              height={47}
              circleColorActive="#000"
              circleColorInactive="#fff"
              backgroundActive="#fff"
              backgroundInactive="#fff"
              style={{ borderColor: "#000", borderWidth: 2 }}
              circleStyle={{ borderColor: "#000", borderWidth: 2 }}
            />
          </View>
          <View style={styles.listItem}>
            <Text style={styles.name}>Instant Email Notifications</Text>
            <Switch
              onSyncPress={value => {
                console.log(value);
              }}
              value={false}
              width={84}
              height={47}
              circleColorActive="#000"
              circleColorInactive="#fff"
              backgroundActive="#fff"
              backgroundInactive="#fff"
              style={{ borderColor: "#000", borderWidth: 2 }}
              circleStyle={{ borderColor: "#000", borderWidth: 2 }}
            />
          </View>
          <View style={styles.listItem}>
            <Text style={styles.name}>Weekly Digest</Text>
            <Switch
              onSyncPress={value => {
                console.log(value);
              }}
              value={false}
              width={84}
              height={47}
              circleColorActive="#000"
              circleColorInactive="#fff"
              backgroundActive="#fff"
              backgroundInactive="#fff"
              style={{ borderColor: "#000", borderWidth: 2 }}
              circleStyle={{ borderColor: "#000", borderWidth: 2 }}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  headerTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#000000",
  },
  container: {
    paddingLeft: 30,
    paddingRight: 30,
  },
  delegateKeyDiv: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 25,
  },
  name: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#000000",
  },
  details: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    paddingTop: 7,
    paddingBottom: 10,
  },
  editIcon: {
    width: 15,
    height: 22,
  },
  settingsText: {
    fontFamily: "Roboto-Bold",
    fontSize: 24,
    color: "#000000",
  },
  listItem: {
    paddingTop: 25,
    paddingBottom: 24,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MemberSettings);
