// @flow

import React, { Component } from "react";
import { Image, ScrollView, StyleSheet, Text, View } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Formik } from "formik";
import { connect } from "react-redux";
import firebase from "react-native-firebase";
import { Button, Item, Input } from "native-base";
import Images from "../../../assets";
import * as actionTypes from "../../../constants/actionTypes";

const mapStateToProps = state => ({ ...state.auth, sdk: state.sdk });

const mapDispatchToProps = dispatch => ({
  onSubmit: values =>
    dispatch({ type: actionTypes.SIGNIN_LOCAL, payload: values }),
  onAuthStateChanged: cb =>
    dispatch({ type: actionTypes.AUTH_STATE_CHANGED, payload: cb }),
});

class OnboardingSigninLocalScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state } = navigation;
    return {
      title: "Sign into existing account",
      headerStyle: {
        backgroundColor: "#fff",
        elevation: 0,
        textAlign: "center",
        borderBottomWidth: 1,
        borderColor: "#000",
      },
      headerTitleStyle: styles.headerTitle,
      headerRight: <View></View>,
    };
  };

  constructor(props) {
    super(props);

    this.authStateChanged = this.authStateChanged.bind(this);
  }

  componentDidMount() {
    this.props.onAuthStateChanged(this.authStateChanged);
  }

  authStateChanged = currentAuthState => {
    if (currentAuthState && currentAuthState.isAnonymous === false) {
      this.props.navigation.navigate("MainNavigator");
    }
  };

  submitForm = values => this.props.onSubmit(values);

  render() {
    return (
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: "center",
          backgroundColor: "#fff",
        }}
      >
        <Formik
          initialValues={{ email: "", password: "" }}
          onSubmit={this.submitForm}
        >
          {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
          }) => (
            <View style={styles.container}>
              <View style={[styles.inputContainer]}>
                <Item style={{ width: "100%" }}>
                  <Image
                    source={Images.EMAIL}
                    resizeMode="contain"
                    style={styles.inputImage}
                  />
                  <Input
                    name="email"
                    style={styles.inputText}
                    onChangeText={text => setFieldValue("email", text)}
                    onBlur={handleBlur}
                    value={values.email}
                    placeholder="Email"
                    placeholderTextColor={inputColor}
                  />
                </Item>
              </View>
              <View style={[styles.inputContainer]}>
                <Item style={{ width: "100%" }}>
                  <Image
                    source={Images.PASSWORD}
                    resizeMode="contain"
                    style={styles.inputImage}
                  />
                  <Input
                    name="password"
                    secureTextEntry={true}
                    onChangeText={text => setFieldValue("password", text)}
                    onBlur={handleBlur}
                    value={values.password}
                    style={styles.inputText}
                    placeholder="Password"
                    placeholderTextColor={inputColor}
                  />
                </Item>
              </View>
              <Text
                style={styles.acctText}
                onPress={() => this.props.navigation.navigate("Signup")}
              >
                I forgot my details
              </Text>
              <Button onPress={handleSubmit} style={styles.btn}>
                <Text style={styles.btnText}>Sign In</Text>
              </Button>
            </View>
          )}
        </Formik>
      </ScrollView>
    );
  }
}

const inputColor = "#C3C3C3";
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    paddingRight: "20%",
    paddingLeft: "20%",
  },
  headerTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    lineHeight: 23,
    textAlign: "center",
    color: "#4C4C4C",
  },
  btn: {
    backgroundColor: "#000000",
    borderRadius: 8,
    width: "80%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 21,
    alignSelf: "center",
  },
  btnText: {
    fontFamily: "Roboto-Regular",
    fontSize: 24,
    textAlign: "center",
    color: "#FFFFFF",
  },
  inputText: {
    fontFamily: "Roboto-Regular",
    fontSize: 18,
  },
  inputContainerParent: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-start",
  },
  inputContainer: {
    flexDirection: "row",
    width: "100%",
    marginBottom: 50,
    borderStyle: "solid",
    borderColor: "#096CEF",
    borderBottomWidth: 2,
  },
  inputImage: {
    width: 40,
    height: 40,
  },
  acctText: {
    fontFamily: "Roboto-Regular",
    fontSize: 18,
    textAlign: "center",
    textDecorationLine: "underline",
    color: "#525252",
    marginTop: 10,
    marginBottom: 70,
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OnboardingSigninLocalScreen);
