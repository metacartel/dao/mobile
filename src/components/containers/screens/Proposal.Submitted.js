import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";
import {
  Container,
  Content,
  Button,
  Footer,
  FooterTab,
  Icon,
} from "native-base";

export default class ProposalSubmitted extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    };
  };

  navigateToProposalDetails = () =>
    this.props.navigation.navigate("ProposalDetails");

  render() {
    return (
      <Container>
        <Content
          contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
        >
          <View style={styles.container}>
            <Text style={styles.header}>Proposal Submitted</Text>
            <Icon
              style={{ color: "#000", fontSize: 160 }}
              name="md-checkmark-circle-outline"
            />
          </View>
        </Content>
        <Footer style={{ backgroundColor: "transparent" }}>
          <FooterTab style={styles.footerTab}>
            <Button onPress={this.navigateToProposalDetails} style={styles.btn}>
              <Text style={styles.btnText}>View Proposal</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  header: {
    fontFamily: "Roboto-Bold",
    fontSize: 24,
    textAlign: "center",
    color: "#000000",
  },
  footerTab: {
    backgroundColor: "transparent",
    paddingLeft: 44,
    paddingRight: 44,
    paddingBottom: 10,
  },
  btn: {
    backgroundColor: "#000000",
    borderRadius: 8,
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 25,
    marginTop: 25,
  },
  btnText: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    textAlign: "center",
    color: "#FFFFFF",
  },
});
