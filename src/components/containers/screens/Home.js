// @flow

import React, { Component } from "react";
import {
  Button,
  Image,
  ImageBackground,
  View,
  ScrollView,
  StyleSheet,
  Text,
} from "react-native";
import DropDownItem from "react-native-drop-down-item";
import { List, ListItem } from "native-base";
import SplashScreen from "react-native-splash-screen";
import { connect } from "react-redux";
import firebase from "react-native-firebase";
import { formatBalance } from "./Utils";
import Images from "../../../assets";
import * as actionTypes from "../../../constants/actionTypes";

const mapStateToProps = state => ({ ...state.home, sdk: state.sdk });

const mapDispatchToProps = dispatch => ({
  onLoad: () => dispatch({ type: actionTypes.HOME_LOADED }),
  onUnload: () => dispatch({ type: actionTypes.HOME_UNLOADED }),
  onSignOut: () => dispatch({ type: actionTypes.SIGNOUT }),
  onEstimateAndDeploy: () => dispatch({ type: actionTypes.DEPLOY_ACCOUNT }),
  onAuthStateChanged: cb =>
    dispatch({ type: actionTypes.AUTH_STATE_CHANGED, payload: cb }),
});

class HomeScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    SplashScreen.hide();
    this.props.onLoad();
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.confirmedSignedOut !== prevProps.confirmedSignedOut &&
      this.props.confirmedSignedOut === true
    ) {
      this.props.navigation.navigate("LoginNavigator");
    }

    if (this.props.sdk !== prevProps.sdk) {
      if (
        this.props.sdk.account &&
        this.props.sdk.account.state === "Created"
      ) {
        this.props.onEstimateAndDeploy();
      }
    }
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  navigateToMemberProfile = params => () =>
    this.props.navigation.navigate("MemberDetails", params);
  navigateToMemberSettings = () =>
    this.props.navigation.navigate("MemberSettings");
  signOut = () => this.props.onSignOut();

  render() {
    const { usdExchangeRate } = this.props || 0;
    const { account } = this.props.sdk;
    const accountAddress =
      account && account.address ? account.address : "undefined";
    const accountAddressSpliced =
      accountAddress.slice(0, 7) +
      "..." +
      accountAddress.slice(-4, accountAddress.length);
    const accountRealBalance = formatBalance(
      account && account.balance.real ? account.balance.real : 0,
      2
    );
    const realBalanceUsd =
      parseFloat(accountRealBalance) * parseFloat(usdExchangeRate);
    const realShares = formatBalance(0);
    const shareCount = 0;

    const memberProfileParams = {
      shares: shareCount,
      tokenTribute: 0,
      item: {},
    };

    const signOutStr = "=> Sign Out";

    return (
      <View style={styles.container}>
        <ScrollView style={{ alignSelf: "stretch" }}>
          <DropDownItem
            style={styles.dropDownItem}
            contentVisible={false}
            invisibleImage={null}
            visibleImage={null}
            header={
              <View
                style={{
                  padding: 5,
                  flexDirection: "row",
                  justifyContent: "space-around",
                  alignItems: "center",
                }}
              >
                <Text style={styles.dropdownHeaderText}>
                  {accountAddressSpliced}
                </Text>
                <Image
                  source={Images.DROPDOWN}
                  style={styles.imageIcon}
                  resizeMode="contain"
                />
              </View>
            }
          >
            <List
              itemDivider={false}
              selected={false}
              noIndent
              style={styles.list}
            >
              <ListItem noBorder style={styles.listItem}>
                <Text
                  onPress={this.navigateToMemberProfile(memberProfileParams)}
                  style={styles.dropdDownListItemText}
                >
                  Profile
                </Text>
              </ListItem>
              <ListItem noBorder style={styles.listItem}>
                <Text
                  onPress={this.navigateToMemberSettings}
                  style={styles.dropdDownListItemText}
                >
                  Settings
                </Text>
              </ListItem>
              <ListItem noBorder style={styles.listItem}>
                <Text style={styles.dropdDownListItemText}>Help</Text>
              </ListItem>
              <ListItem noBorder style={styles.listItem}>
                <Text
                  onPress={this.signOut}
                  style={styles.dropdDownListItemText}
                >
                  {signOutStr}
                </Text>
              </ListItem>
            </List>
          </DropDownItem>
        </ScrollView>
        <View style={styles.subContainer}>
          <Image
            source={Images.METACARTEL}
            style={styles.imageLogo}
            resizeMode="contain"
          />
          <Text style={styles.header}>MetaCartel DAO</Text>
          <Text style={styles.subheader}>
            If you want to go quick, go alone.
          </Text>
          <Text style={styles.subheader}>
            If you want to go far, go together.
          </Text>
          <Text style={styles.title}>Share Value</Text>
          <Text style={styles.subTitle}>Ξ {realShares}</Text>
          <Text style={styles.desc}> </Text>
          <ImageBackground
            source={Images.GRAPH}
            style={{
              width: "100%",
              height: 120,
              justifyContent: "space-around",
              alignItems: "flex-end",
              flexDirection: "row",
            }}
            resizeMode="stretch"
          >
            <View
              style={{
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center",
              }}
            >
              <Text style={styles.title}>Bank</Text>
              <Text style={styles.subTitle}>Ξ {accountRealBalance}</Text>
              <Text style={styles.value}>$ {realBalanceUsd}</Text>
            </View>
            <View
              style={{
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center",
                top: 0,
              }}
            >
              <Text style={styles.title}>Shares</Text>
              <Text style={styles.subTitle}>{shareCount}</Text>
            </View>
          </ImageBackground>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  subContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    position: "absolute",
    top: 50,
    width: "100%",
    marginLeft: 10,
    zIndex: -1,
  },
  dropDownItem: {
    width: "40%",
    alignSelf: "flex-end",
    borderWidth: 2,
    borderColor: "#000",
    borderRadius: 30,
    margin: 0,
    padding: 0,
    backgroundColor: "#fff",
  },
  list: {
    margin: 0,
    padding: 0,
  },
  listItem: {
    padding: 0,
    margin: 0,
    width: "100%",
  },
  imageIcon: {
    width: 10,
    height: 10,
  },
  dropdownHeaderText: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
  },
  dropdDownListItemText: {
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    color: "#000000",
    margin: 0,
    padding: 0,
  },
  imageLogo: {
    width: 144,
    height: 144,
  },
  header: {
    fontFamily: "Roboto-Bold",
    fontSize: 32,
    color: "#000000",
    marginBottom: 30,
    textAlign: "center",
  },
  subheader: {
    fontFamily: "Roboto-Bold",
    fontSize: 21,
    color: "#000000",
    textAlign: "center",
  },
  title: {
    fontFamily: "Roboto-Bold",
    fontSize: 18,
    textAlign: "center",
    marginTop: 30,
    color: "#000000",
  },
  subTitle: {
    fontFamily: "Roboto-Regular",
    fontSize: 24,
    color: "rgba(0, 0, 0, 0.82)",
    textAlign: "center",
  },
  desc: {
    fontFamily: "Roboto-Regular",
    fontSize: 14,
    textAlign: "center",
    color: "#EF495F",
  },
  value: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "rgba(0, 0, 0, 0.82)",
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);
