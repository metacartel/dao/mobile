// @flow

import React, { Component } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import SplashScreen from "react-native-splash-screen";
import Icon from "react-native-vector-icons/FontAwesome5";
import { connect } from "react-redux";
import { Button } from "native-base";
import * as actionTypes from "../../../constants/actionTypes";
import Images from "../../../assets";
import firebase from "react-native-firebase";

const mapStateToProps = state => ({ ...state.auth, sdk: state.sdk });

const mapDispatchToProps = dispatch => ({
  onBootGoogle: () => dispatch({ type: actionTypes.BOOT_GOOGLE }),
  onLoginWithGoogle: () => dispatch({ type: actionTypes.LOGIN_WITH_GOOGLE }),
  onLoginWithFacebook: () =>
    dispatch({ type: actionTypes.LOGIN_WITH_FACEBOOK }),
  onLoginWithTwitter: () => dispatch({ type: actionTypes.LOGIN_WITH_TWITTER }),
  onAuthStateChanged: cb =>
    dispatch({ type: actionTypes.AUTH_STATE_CHANGED, payload: cb }),
});

class OnboardingScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.authStateChanged = this.authStateChanged.bind(this);
  }

  componentDidMount() {
    this.props.onBootGoogle();
    this.props.onAuthStateChanged(this.authStateChanged);
  }

  authStateChanged = currentAuthState => {
    const { navigate } = this.props.navigation;
    if (currentAuthState && currentAuthState.isAnonymous === false) {
      let navAction = setInterval(() => {
        if (firebase.auth().currentUser !== null) {
          clearInterval(navAction);
          navigate("MainNavigator");
        }
      }, 500);
    } else {
      SplashScreen.hide();
    }
  };

  loginWithGoogle = () => this.props.onLoginWithGoogle();
  loginWithFacebook = () => this.props.onLoginWithFacebook();
  loginWithTwitter = () => this.props.onLoginWithTwitter();

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={Images.STARSTRUCK}
          style={styles.imageIcon}
          resizeMode="contain"
        />
        <Text style={styles.header}>MetaCartel DAO</Text>
        <Text style={styles.subheader}>Create new account</Text>
        <Text style={styles.desc}>
          Your contact details will only be used in case you need to recover
          your account.
        </Text>
        <Button style={styles.btn} onPress={this.loginWithGoogle}>
          <Text style={styles.btnText}>Sign up with Google</Text>
        </Button>
        <Button style={styles.btn} onPress={this.loginWithFacebook}>
          <Text style={styles.btnText}>Sign up with Facebook</Text>
        </Button>
        <Button
          disabled={true}
          style={styles.btn}
          onPress={this.loginWithTwitter}
        >
          <Text style={styles.btnText}>Sign up with Twitter</Text>
        </Button>
        <Button
          style={styles.btn}
          onPress={() => this.props.navigation.navigate("Signup")}
        >
          <Text style={styles.btnText}>Sign up with Email</Text>
        </Button>
        <Text
          style={styles.acctText}
          onPress={() => this.props.navigation.navigate("SigninLocal")}
        >
          I already have an account
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 60,
    paddingRight: 60,
    alignItems: "center",
  },
  header: {
    fontFamily: "Roboto-Bold",
    fontSize: 36,
    color: "#000",
    marginBottom: 20,
  },
  subheader: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#4C4C4C",
    marginBottom: 6,
  },
  imageIcon: {
    width: 44,
    height: 44,
  },
  desc: {
    fontFamily: "Roboto-Regular",
    fontSize: 14,
    lineHeight: 16,
    textAlign: "center",
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 30,
  },
  btn: {
    backgroundColor: "#000000",
    borderRadius: 8,
    width: "100%",
    height: "10%",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 21,
  },
  btnText: {
    fontFamily: "Roboto-Regular",
    fontSize: 24,
    textAlign: "center",
    color: "#FFFFFF",
  },
  acctText: {
    fontFamily: "Roboto-Regular",
    fontSize: 18,
    textAlign: "center",
    textDecorationLine: "underline",
    color: "#525252",
    marginTop: 10,
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OnboardingScreen);
