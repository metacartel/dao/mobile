import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  Image,
  Animated,
  TouchableOpacity,
  TouchableHighlight,
  Modal,
} from "react-native";
import DropDownItem from "react-native-drop-down-item";
import { Button, Icon, Thumbnail } from "native-base";
import { weiToEth } from "@netgum/utils";
import BN from "bn.js";
import Images from "../../../assets";
import Video from "react-native-video";

const HEADER_MAX_HEIGHT = 130;
const HEADER_MIN_HEIGHT = 0;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const videoUrl = "http://techslides.com/demos/sample-videos/small.mp4";

export default class ProposalDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(0),
      player: null,
      modalVisible: false,
    };
  }

  static navigationOptions = ({ navigation }) => {
    const navigateHome = () => navigation.navigate("Home");

    return {
      title: "Proposal",
      headerStyle: {
        backgroundColor: "#fff",
        elevation: 0,
        textAlign: "center",
        borderBottomWidth: 1,
        borderColor: "#000",
      },
      headerTitleStyle: styles.headerTitle,
      headerRight: <View />,
      headerLeft: (
        <Button transparent onPress={navigateHome}>
          <Icon style={{ color: "#000", paddingTop: 10 }} name="arrow-back" />
        </Button>
      ),
    };
  };

  navigateVideoFullscreen = () =>
    this.props.navigation.navigate("VideoFullScreen");

  formatBalance(balance) {
    return balance ? weiToEth(balance).toFixed(6) : 0;
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: "clamp",
    });

    const item = this.props.navigation.getParam("item", null);
    const { sharesRequested, tokenTribute, timestamp } = item;
    const tokensBN = new BN(tokenTribute, 10);
    const name = "undefined";

    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          scrollEventThrottle={1}
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: { y: this.state.scrollY },
              },
            },
          ])}
        >
          <>
            <Button
              style={{ alignSelf: "flex-end", marginRight: 25 }}
              transparent
            >
              <Image
                source={Images.STAR}
                resizeMode="contain"
                style={styles.starImage}
              />
            </Button>
            <View style={styles.container}>
              <Text style={styles.header}>{name}</Text>
              <View style={styles.tabTable}>
                <View>
                  <Text style={styles.subheader}>Tribute</Text>
                  <Text style={styles.value}>
                    Ξ {this.formatBalance(tokensBN)}
                  </Text>
                </View>
                <View>
                  <Text style={styles.subheader}>Shares Requested</Text>
                  <Text style={styles.value}>{sharesRequested}</Text>
                </View>
              </View>
              <View style={{ marginTop: 23 }}>
                <Text style={styles.votingPeriod}>Voting Period</Text>
                <View style={{ flexDirection: "row" }}>
                  <Image
                    style={styles.clockIcon}
                    source={Images.CLOCK}
                    resizeMode="contain"
                  ></Image>
                  <Text style={styles.votingPeriod}>
                    1 day 4 hours 48 minutes
                  </Text>
                </View>
              </View>
            </View>
            <TouchableOpacity
              style={{ marginTop: 27, marginBottom: 16 }}
              activeOpacity={1}
              onPress={this.navigateVideoFullscreen}
            >
              <Video
                source={{ uri: videoUrl }}
                ref={ref => {
                  this.player = ref;
                }}
                controls={true}
                resizeMode="cover"
                style={styles.backgroundVideo}
              />
            </TouchableOpacity>
            <View style={styles.container}>
              <View>
                <Text style={styles.heading}>Summary</Text>
                <Text style={styles.description}>
                  Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur.
                </Text>
              </View>
              <View>
                <Text style={styles.heading}>Heading</Text>
                <Text style={styles.description}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                  irure dolor in reprehenderit in voluptate velit esse cillum
                  dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                  cupidatat non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.
                </Text>
              </View>
            </View>
          </>
        </ScrollView>

        <Animated.View
          style={[
            styles.footer,
            { height: headerHeight, paddingLeft: 19, paddingRight: 19 },
          ]}
        >
          <TouchableHighlight
            onPress={() => {
              this.setModalVisible(true);
            }}
            style={[
              styles.bar,
              { flexDirection: "column", alignItems: "center" },
            ]}
          >
            <>
              <View
                style={{
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center",
                  paddingLeft: 25,
                  paddingRight: 25,
                }}
              >
                <View style={styles.voterTextDiv}>
                  <Text style={styles.voterText}>120</Text>
                </View>
                <View
                  style={{
                    width: "60%",
                    flexDirection: "row",
                    justifyContent: "center",
                  }}
                >
                  <Image
                    source={Images.UP}
                    style={styles.arrowIcon}
                    resizeMode="contain"
                  />
                </View>
                <View style={styles.voterTextDiv}>
                  <Text style={styles.voterText}>200</Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  width: "100%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <View style={styles.bottomImageDiv}>
                  <Image
                    source={Images.STARSTRUCK}
                    style={styles.imageIcon}
                    resizeMode="contain"
                  />
                </View>
                <View
                  style={{
                    width: "60%",
                    flexDirection: "row",
                    justifyContent: "space-around",
                  }}
                >
                  <View style={styles.greenLine} />
                  <View style={styles.redLine} />
                </View>
                <View style={styles.bottomImageDiv}>
                  <Image
                    source={Images.THROWUP}
                    style={styles.imageIcon}
                    resizeMode="contain"
                  />
                </View>
              </View>
            </>
          </TouchableHighlight>
        </Animated.View>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}
        >
          <View style={{ marginTop: 15 }}>
            <View>
              <TouchableHighlight
                style={{
                  height: 50,
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center",
                }}
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}
              >
                <Image
                  source={Images.DOWN}
                  style={styles.arrowIcon}
                  resizeMode="contain"
                />
              </TouchableHighlight>
              <Text
                style={[styles.header, { paddingLeft: 30, paddingRight: 30 }]}
              >
                Votes By Member
              </Text>
              <View style={styles.line} />
              <>
                <View style={styles.listItem}>
                  <View
                    style={{
                      width: "15%",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Thumbnail
                      circle
                      medium
                      source={{
                        uri:
                          "https://facebook.github.io/react-native/docs/assets/favicon.png",
                      }}
                    />
                  </View>
                  <View style={{ width: "70%", paddingLeft: 19 }}>
                    <Text style={[styles.headerTitle]}>Pseudonym</Text>
                    <Text style={styles.desc}>0xd26b...45B9</Text>
                    <Text style={styles.subheader}>10 Shares</Text>
                  </View>
                  <View
                    style={{
                      width: "15%",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Image
                      source={Images.STARSTRUCK}
                      style={styles.imageModalIcon}
                      resizeMode="contain"
                    />
                  </View>
                </View>
                <View style={styles.listItem}>
                  <View
                    style={{
                      width: "15%",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Thumbnail
                      circle
                      medium
                      source={{
                        uri:
                          "https://facebook.github.io/react-native/docs/assets/favicon.png",
                      }}
                    />
                  </View>
                  <View style={{ width: "70%", paddingLeft: 19 }}>
                    <Text style={[styles.headerTitle]}>Pseudonym</Text>
                    <Text style={styles.desc}>0xd26b...45B9</Text>
                    <Text style={styles.subheader}>10 Shares</Text>
                  </View>
                  <View
                    style={{
                      width: "15%",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Image
                      source={Images.THROWUP}
                      style={styles.imageModalIcon}
                      resizeMode="contain"
                    />
                  </View>
                </View>
              </>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // paddingTop: 10,
    paddingBottom: 17,
    paddingLeft: 45,
    paddingRight: 45,
  },
  headerTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#000000",
  },
  starImage: {
    height: 28,
    width: 30,
  },
  header: {
    fontFamily: "Roboto-Bold",
    fontSize: 24,
    color: "#000000",
  },
  tabTable: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 25,
  },
  subheader: {
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    color: "#000000",
  },
  value: {
    fontFamily: "Roboto-Regular",
    fontSize: 30,
    color: "rgba(0, 0, 0, 0.82)",
  },
  votingPeriod: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
  },
  clockIcon: {
    width: 20,
    height: 20,
    marginRight: 15,
  },
  footer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "#fff",
  },
  bar: {
    marginTop: 25,
    alignItems: "center",
    justifyContent: "center",
  },
  heading: {
    fontFamily: "Roboto-Bold",
    fontSize: 21,
    color: "#000000",
    marginBottom: 20,
  },
  description: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    marginBottom: 20,
  },
  backgroundVideo: {
    height: 233,
    width: "100%",
  },
  imageIcon: {
    width: 30,
    height: 30,
  },
  greenLine: {
    backgroundColor: "#45D721",
    height: 4,
    width: "65%",
  },
  redLine: {
    backgroundColor: "#000",
    height: 4,
    width: "35%",
  },
  voterTextDiv: {
    width: "25%",
    height: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  voterText: {
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    textAlign: "center",
    color: "#000000",
    marginBottom: 5,
  },
  bottomImageDiv: {
    padding: 15,
    borderWidth: 2,
    borderColor: "#000",
    borderRadius: 25,
    width: 50,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
  },
  listItem: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 20,
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderColor: "rgba(0, 0, 0, 0.15)",
    flexDirection: "row",
    alignItems: "center",
  },
  desc: {
    fontFamily: "Roboto-Normal",
    fontSize: 20,
    color: "#000000",
  },
  line: {
    backgroundColor: "rgba(0, 0, 0, 0.15)",
    height: 2,
    width: "100%",
  },
  imageModalIcon: {
    width: 48,
    height: 48,
    borderRadius: 24,
  },
  arrowIcon: {
    width: 22,
    height: 15,
  },
});
