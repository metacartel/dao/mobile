import React, { Component } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import {
  Body,
  Button,
  Container,
  Content,
  Footer,
  FooterTab,
  Header,
  Icon,
  Left,
  List,
  ListItem,
  ScrollableTab,
  Tab,
  Tabs,
  Input,
} from "native-base";
import Images from "../../../assets";

export default class AddProposal extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state } = navigation;
    const navigateBack = () => navigation.goBack();

    return {
      title: "New Proposal",
      headerStyle: {
        backgroundColor: "#fff",
        elevation: 0,
        textAlign: "center",
        borderBottomWidth: 1,
        borderColor: "#000",
      },
      headerTitleStyle: styles.headerTitle,
      headerRight: (
        <View>
          <Button transparent onPress={navigateBack}>
            <Icon style={{ color: "#000" }} name="close" />
          </Button>
        </View>
      ),
    };
  };

  navigateToAddProposal = () => this.props.navigation.navigate("AddProposal");
  navigateToPropsalSubmitted = () =>
    this.props.navigation.navigate("ProposalSubmitted");

  render() {
    return (
      <Container>
        <View style={styles.container}>
          <>
            <View style={styles.section1}>
              <View>
                <Text style={styles.subheader}>Proposal Title</Text>
                <View>
                  <Input
                    style={{
                      marginTop: 0,
                      lineHeight: 2,
                      backgroundColor: "#000000",
                    }}
                    placeHolder="New proposal title"
                  />
                </View>
              </View>
              <View style={styles.tabTable}>
                <View>
                  <Text style={styles.subheader}>Tribute</Text>
                  <View>
                    <Input
                      style={{ backgroundColor: "#000000" }}
                      placeHolder="10"
                      keyboardType="numeric"
                    />
                  </View>
                </View>
                <View>
                  <Text style={styles.subheader}>Shares Requested</Text>
                  <View>
                    <Input
                      style={{ backgroundColor: "#000000" }}
                      placeHolder="10"
                      keyboardType="numeric"
                    />
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.line} />
            <View style={styles.section2}>
              <View>
                <Text style={[styles.ytLink, { marginBottom: 10 }]}>
                  Insert link to youtube
                </Text>
                <Input
                  style={{ paddingVertical: 0 }}
                  placeholder="https://youtube.com/embed/xhWye6"
                />
              </View>
              <Button
                style={{ alignSelf: "flex-end" }}
                transparent
                onPress={this.navigateToAddProposal}
              >
                <Image
                  style={styles.plusIcon}
                  source={Images.PLUS}
                  resizeMode="contain"
                ></Image>
              </Button>
            </View>
            <View style={styles.line} />
            <View
              style={[
                styles.section2,
                { height: "30%", alignItems: "flex-start" },
              ]}
            >
              <Input multiline={true} placeholder="Insert links or content" />
            </View>
            <View style={styles.line} />
          </>
        </View>
        <Content />
        <Footer style={{ backgroundColor: "transparent" }}>
          <FooterTab
            style={{
              backgroundColor: "transparent",
              paddingLeft: 44,
              paddingRight: 44,
              paddingBottom: 60,
            }}
          >
            <Button
              onPress={this.navigateToProposalSubmitted}
              style={styles.btn}
            >
              <Text style={styles.btnText}>Submit Proposal</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-between",
    flexDirection: "column",
  },
  section1: {
    padding: 44,
    paddingTop: 50,
    paddingBottom: 60,
  },
  section2: {
    padding: 44,
    paddingTop: 17,
    paddingBottom: 17,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  headerTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#000000",
  },
  header: {
    fontFamily: "Roboto-Bold",
    fontSize: 24,
    color: "#000000",
  },
  tabTable: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 25,
  },
  subheader: {
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    color: "#000000",
  },
  value: {
    fontFamily: "Roboto-Regular",
    fontSize: 30,
    color: "rgba(0, 0, 0, 0.82)",
  },
  line: {
    backgroundColor: "#000",
    height: 2,
    width: "100%",
  },
  ytLink: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
  },
  plusIcon: {
    height: 21,
    width: 21,
  },
  btn: {
    backgroundColor: "#000000",
    borderRadius: 8,
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 25,
    marginTop: 25,
  },
  btnText: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    textAlign: "center",
    color: "#FFFFFF",
  },
});
