import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Body, Button, Header, Icon, Left, Right } from "native-base";
import Video from "react-native-video";

const videoUrl = "http://techslides.com/demos/sample-videos/small.mp4";

export default class VideoFullScreen extends Component {
  render() {
    return (
      <>
        <Header hasTabs style={{ backgroundColor: "#000" }}>
          <Left />
          <Body />
          <Right>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon style={{ color: "#fff", fontSize: 35 }} name="close" />
            </Button>
          </Right>
        </Header>
        <Video
          source={{ uri: videoUrl }} // Can be a URL or a local file.
          ref={ref => {
            this.player = ref;
          }} // Store reference
          //    onBuffer={this.onBuffer}                // Callback when remote video is buffering
          //     onError={this.videoError}               // Callback when video cannot be loaded
          controls={true}
          resizeMode="contain"
          style={styles.backgroundVideo}
        />
      </>
    );
  }
}
const styles = StyleSheet.create({
  backgroundVideo: {
    position: "absolute",
    top: 50,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: "#000",
  },
});
