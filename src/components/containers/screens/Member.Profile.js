import React, { Component } from "react";
import {
  Clipboard,
  Image,
  View,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from "react-native";
import { Button, Icon, Thumbnail } from "native-base";
import DropDownItem from "react-native-drop-down-item";
import { weiToEth } from "@netgum/utils";
import BN from "bn.js";
import Identicon from "identicon.js";
import moment from "moment";
import Images from "../../../assets";

export default class MemberProfile extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Member",
      headerStyle: {
        backgroundColor: "#fff",
        elevation: 0,
        textAlign: "center",
        borderBottomWidth: 1,
        borderColor: "#000",
      },
      headerTitleStyle: styles.headerTitle,
      headerRight: <View />,
    };
  };

  formatBalance(balance) {
    return balance ? weiToEth(balance).toFixed(6) : 0;
  }

  copyToClipboard = address => () => {
    Clipboard.setString(address);
  };

  render() {
    const address = this.props.navigation.getParam("address", "undefined");
    const tribute = this.props.navigation.getParam("tribute", "0");
    const shares = this.props.navigation.getParam("shares", "0");
    const details = this.props.navigation.getParam("item", null);

    const avatar = new Identicon(
      "d3b07384d113edec49eaa6238ad5ff00",
      420
    ).toString();
    const avatarUri = `data:image/png;base64,${avatar}`;
    const votes = details.votes || [1, 2];
    const name = "undefined";

    return (
      <ScrollView
        contentContainerStyle={{ flexGrow: 1, justifyContent: "flex-start" }}
      >
        <View style={styles.container}>
          <TouchableOpacity
            onPress={this.copyToClipboard}
            style={styles.section1}
          >
            <Thumbnail circle medium source={{ uri: avatarUri }} />
            <View style={{ paddingLeft: 20 }}>
              <Text style={styles.name}>{name}</Text>
              <View
                style={[
                  { flexDirection: "row", justifyContent: "space-between" },
                ]}
              >
                <Text style={styles.copy}>{address}</Text>
                <Text style={styles.copy}> Copy</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.section2}>
            <View style={{ width: "50%" }}>
              <Text style={[styles.tabTitle]}>Tribute</Text>
              <Text style={styles.value}>Ξ {tribute}</Text>
            </View>
            <View style={{ width: "50%" }}>
              <Text style={styles.tabTitle}>Shares</Text>
              <Text style={styles.value}>{shares}</Text>
            </View>
          </View>
          <View style={styles.delegateKeyDiv}>
            <View>
              <Text style={styles.name}>Delegate Key</Text>
              <Text style={styles.details}>{address}</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text style={[styles.details, { marginRight: 10 }]}>Edit</Text>
              <Image
                style={styles.editIcon}
                source={Images.RIGHT}
                resizeMode="contain"
              />
            </View>
          </View>
          <View
            style={[
              styles.dropdownTitleDiv,
              { marginTop: 40, marginBottom: 20 },
            ]}
          >
            <Text style={styles.dropdownTitle}>Vote History</Text>
          </View>
        </View>
        <View style={styles.line} />
        <>
          {votes.map((item, index) => {
            const processed = item.proposal.processed ? "| Processed" : null;
            const { sharesRequested, tokenTribute, timestamp } = item.proposal;
            const tokensBN = new BN(tokenTribute, 10);
            const date = moment.unix(timestamp).format("YYYY-MM-DD");

            return (
              <View key={index} style={styles.listItem}>
                <View style={{ width: "85%" }}>
                  <Text style={[styles.desc, { marginBottom: 9 }]}>{date}</Text>
                  <Text style={styles.headerText}>undefined</Text>
                  <Text style={styles.desc}>
                    Ξ {this.formatBalance(tokensBN)} Tribute | {sharesRequested}{" "}
                    Shares {processed}
                  </Text>
                </View>
                <View
                  style={{
                    width: "15%",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Image
                    source={Images.STARSTRUCK}
                    style={styles.imageIcon}
                    resizeMode="contain"
                  />
                </View>
              </View>
            );
          })}
        </>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: 30,
    paddingRight: 30,
  },
  headerTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#000000",
  },
  starImage: {
    width: 30,
    height: 30,
  },
  thumbnail: {
    width: 100,
    height: 100,
  },
  section1: {
    paddingTop: 35,
    paddingBottom: 35,
    flexDirection: "row",
    alignItems: "center",
    // justifyContent: 'center'
  },
  name: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#000000",
  },
  details: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    paddingTop: 7,
    paddingBottom: 10,
  },
  copy: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
  },
  section2: {
    flexDirection: "row",
    // justifyContent: 'space-evenly'
  },
  tabTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    color: "#000000",
  },
  value: {
    fontFamily: "Roboto-Regular",
    fontSize: 30,
    color: "rgba(0, 0, 0, 0.82)",
  },
  dropDownItem: {
    // paddingTop: 5,
    // paddingBottom: 5,
    // backgroundColor: 'red',
    // height: 50
  },
  delegateKeyDiv: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 25,
  },
  editIcon: {
    width: 15,
    height: 22,
  },
  dropdownTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: 24,
    color: "#000",
  },
  dropdownTitleDiv: {
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },

  line: {
    backgroundColor: "rgba(0, 0, 0, 0.15)",
    height: 2,
    width: "100%",
  },
  listItem: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 20,
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderColor: "rgba(0, 0, 0, 0.15)",
    flexDirection: "row",
    alignItems: "center",
  },
  headerText: {
    fontFamily: "Roboto-Bold",
    fontSize: 21,
    color: "#000000",
  },
  imageIcon: {
    width: 48,
    height: 48,
  },
  desc: {
    fontFamily: "Roboto-Normal",
    fontSize: 16,
    color: "#000000",
  },
});
