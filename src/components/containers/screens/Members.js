import React, { Component } from "react";
import {
  View,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from "react-native";
import {
  Body,
  Button,
  Header,
  Icon,
  Left,
  List,
  ListItem,
  ScrollableTab,
  Tab,
  Tabs,
  Thumbnail,
} from "native-base";
import { connect } from "react-redux";
import BN from "bn.js";
import Identicon from "identicon.js";
import { formatBalance } from "./Utils";
import * as actionTypes from "../../../constants/actionTypes";

const mapStateToProps = state => ({ ...state.members, sdk: state.sdk });

const mapDispatchToProps = dispatch => ({
  onLoad: () => dispatch({ type: actionTypes.MEMBERS_LOADED }),
  onUnload: () => dispatch({ type: actionTypes.MEMBERS_UNLOADED }),
});

class Members extends Component {
  componentDidMount() {
    this.props.onLoad();
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  navigateHome = () => this.props.navigation.navigate("HomeTab");

  render() {
    const members = this.props.members || [];

    return (
      <>
        <Header hasTabs style={{ backgroundColor: "transparent" }}>
          <Left>
            <Button transparent onPress={this.navigateHome}>
              <Icon style={{ color: "#000" }} name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Text style={styles.headerText}>Members</Text>
          </Body>
        </Header>
        <ScrollView>
          <View style={styles.container}>
            <>
              <Text style={[styles.title, styles.sections]}>
                Elders (100+ Shares)
              </Text>
              {members.map((item, index) => {
                const { delegateKey, shares, tokenTribute } = item;
                const address =
                  delegateKey.slice(0, 5) +
                  "..." +
                  delegateKey.slice(-4, item.length);
                const tokensBN = new BN(tokenTribute, 10);
                const tribute = formatBalance(tokensBN, 2);
                const avatar = new Identicon(delegateKey, 420).toString();
                const avatarUri = `data:image/png;base64,${avatar}`;
                const name = "undefined";

                return (
                  <TouchableOpacity
                    key={index}
                    onPress={() =>
                      this.props.navigation.navigate("MemberProfile", {
                        address,
                        shares,
                        tribute,
                        item,
                      })
                    }
                    style={[styles.listItem, { borderTopWidth: 1 }]}
                  >
                    <Thumbnail
                      circle
                      style={styles.thumbnail}
                      source={{ uri: avatarUri }}
                    />
                    <View style={{ paddingLeft: 35 }}>
                      <Text style={styles.headerText}>{name}</Text>
                      <Text style={styles.subheader}>{address}</Text>
                      <Text style={styles.desc}>
                        Ξ {tribute} Tribute | {shares} Shares
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </>
          </View>
        </ScrollView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  sections: {
    marginTop: 60,
  },
  headerText: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#000000",
  },
  title: {
    fontFamily: "Roboto-Bold",
    fontSize: 24,
    color: "#000000",
    paddingLeft: 30,
    paddingRight: 30,
    marginBottom: 23,
  },
  listItem: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 35,
    paddingBottom: 35,
    borderBottomWidth: 1,
    borderColor: "rgba(0, 0, 0, 0.15);",
    flexDirection: "row",
    alignItems: "center",
  },
  subheader: {
    fontFamily: "Roboto-Regular",
    fontSize: 20,
    color: "#000000",
    paddingTop: 5,
    paddingBottom: 5,
  },
  desc: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Members);
