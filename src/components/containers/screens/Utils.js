import { weiToEth } from "@netgum/utils";

export function formatBalance(balance, decimals) {
  return balance ? weiToEth(balance).toFixed(decimals) : 0;
}
