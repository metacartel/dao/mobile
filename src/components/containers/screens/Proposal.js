import React, { Component } from "react";
import {
  Image,
  ImageBackground,
  View,
  ScrollView,
  StyleSheet,
  Text,
} from "react-native";
import DropDownItem from "react-native-drop-down-item";
import {
  Body,
  Button,
  Header,
  Icon,
  Left,
  List,
  ListItem,
  ScrollableTab,
  Tab,
  Tabs,
} from "native-base";
import { connect } from "react-redux";
import { weiToEth } from "@netgum/utils";
import BN from "bn.js";
import Images from "../../../assets";
import * as actionTypes from "../../../constants/actionTypes";

const mapStateToProps = state => ({ ...state.proposals, sdk: state.sdk });

const mapDispatchToProps = dispatch => ({
  onLoad: () => dispatch({ type: actionTypes.PROPOSALS_LOADED }),
  onUnload: () => dispatch({ type: actionTypes.PROPOSALS_UNLOADED }),
});

class ProposalScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onLoad();
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  navigateHome = () => this.props.navigation.navigate("HomeTab");

  formatBalance(balance) {
    return balance ? weiToEth(balance).toFixed(6) : 0;
  }

  render() {
    const proposals = this.props.proposals || [];
    const proposalsAborted = proposals.filter(
      item => item && item.aborted === true
    );

    return (
      <>
        <Header hasTabs style={{ backgroundColor: "transparent" }}>
          <Left>
            <Button transparent onPress={this.navigateHome}>
              <Icon style={{ color: "#000" }} name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Text style={styles.headerText}>Proposals</Text>
          </Body>
        </Header>
        <Tabs
          style={{ paddingTop: 10, paddingBottom: 10 }}
          renderTabBar={() => (
            <ScrollableTab style={{ backgroundColor: "transparent" }} />
          )}
        >
          <Tab
            activeTextStyle={styles.activeTextStyle}
            activeTabStyle={styles.activeTab}
            tabStyle={styles.tab}
            textStyle={styles.tabText}
            heading="Voting Period"
          >
            <Button
              style={{ alignSelf: "flex-end", marginTop: 10 }}
              transparent
              onPress={() => this.props.navigation.navigate("AddProposal")}
            >
              <Image
                style={styles.plusIcon}
                source={Images.PLUS}
                resizeMode="contain"
              ></Image>
            </Button>
            <ScrollView>
              {proposals.map((item, index) => {
                const {
                  yesVotes,
                  noVotes,
                  sharesRequested,
                  tokenTribute,
                  proposalIndex,
                } = item;
                const tokensBN = new BN(tokenTribute, 10);
                const yesVotesN = parseInt(yesVotes);
                const noVotesN = parseInt(noVotes);
                const totalVotes = yesVotesN + noVotesN;
                let percentGreen = 50;
                let percentRed = 50;

                if (yesVotesN > 0 && noVotesN > 0) {
                  percentGreen = (yesVotesN * 100) / totalVotes;
                  percentRed = (noVotesN * 100) / totalVotes;
                } else if (yesVotesN > 0 && noVotesN === 0) {
                  percentGreen = 100;
                  percentRed = 0;
                } else if (yesVotesN === 0 && noVotesN > 0) {
                  percentGreen = 0;
                  percentRed = 100;
                }

                return (
                  <View
                    key={index}
                    style={[styles.container, { paddingTop: 0 }]}
                  >
                    <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                      <Text style={styles.header}>undefined</Text>
                      <Text style={styles.header}>undefined</Text>
                      <View style={styles.tabTable}>
                        <View>
                          <Text style={styles.subheader}>Tribute</Text>
                          <Text style={styles.value}>
                            Ξ {this.formatBalance(tokensBN)}
                          </Text>
                        </View>
                        <View>
                          <Text style={styles.subheader}>Shares Requested</Text>
                          <Text style={styles.value}>{sharesRequested}</Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: "row", marginTop: 40 }}>
                        <Image
                          style={styles.clockIcon}
                          source={Images.CLOCK}
                          resizeMode="contain"
                        ></Image>
                        <Text style={styles.days}>startingPeriod</Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          marginTop: 40,
                          justifyContent: "space-around",
                        }}
                      >
                        <View>
                          <Text style={styles.yes}>Yes</Text>
                          <Text style={styles.yes}>{yesVotes}</Text>
                        </View>
                        <View>
                          <Text style={styles.no}>No</Text>
                          <Text style={styles.no}>{noVotes}</Text>
                        </View>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-around",
                          marginTop: 10,
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: "#45D721",
                            height: 4,
                            width: `${percentGreen}%`,
                          }}
                        />
                        <View
                          style={{
                            backgroundColor: "#FF0000",
                            height: 4,
                            width: `${percentRed}%`,
                          }}
                        />
                      </View>
                      <Button
                        onPress={() =>
                          this.props.navigation.navigate("ProposalDetails", {
                            proposalIndex,
                            item,
                          })
                        }
                        style={styles.btn}
                      >
                        <Text style={styles.btnText}>View Proposal</Text>
                      </Button>
                    </View>
                  </View>
                );
              })}
            </ScrollView>
          </Tab>
          <Tab
            activeTextStyle={styles.activeTextStyle}
            activeTabStyle={styles.activeTab}
            tabStyle={styles.tab}
            textStyle={styles.tabText}
            heading="Grace Period"
          >
            <View style={{ alignSelf: "flex-end", marginTop: 10 }}></View>
            <ScrollView>
              {proposals.map((item, index) => {
                const {
                  yesVotes,
                  noVotes,
                  sharesRequested,
                  tokenTribute,
                  proposalIndex,
                } = item;
                const tokensBN = new BN(tokenTribute, 10);
                const yesVotesN = parseInt(yesVotes);
                const noVotesN = parseInt(noVotes);
                const totalVotes = yesVotesN + noVotesN;
                let percentGreen = 50;
                let percentRed = 50;

                if (yesVotesN > 0 && noVotesN > 0) {
                  percentGreen = (yesVotesN * 100) / totalVotes;
                  percentRed = (noVotesN * 100) / totalVotes;
                } else if (yesVotesN > 0 && noVotesN === 0) {
                  percentGreen = 100;
                  percentRed = 0;
                } else if (yesVotesN === 0 && noVotesN > 0) {
                  percentGreen = 0;
                  percentRed = 100;
                }

                return (
                  <View
                    key={index}
                    style={[styles.container, { paddingTop: 0 }]}
                  >
                    <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                      <Text style={styles.header}>undefined</Text>
                      <Text style={styles.header}>undefined</Text>
                      <View style={styles.tabTable}>
                        <View>
                          <Text style={styles.subheader}>Tribute</Text>
                          <Text style={styles.value}>
                            Ξ {this.formatBalance(tokensBN)}
                          </Text>
                        </View>
                        <View>
                          <Text style={styles.subheader}>Shares Requested</Text>
                          <Text style={styles.value}>{sharesRequested}</Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: "row", marginTop: 40 }}>
                        <Image
                          style={styles.clockIcon}
                          source={Images.CLOCK}
                          resizeMode="contain"
                        ></Image>
                        <Text style={styles.days}>startingPeriod</Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          marginTop: 40,
                          justifyContent: "space-around",
                        }}
                      >
                        <View>
                          <Text style={styles.yes}>Yes</Text>
                          <Text style={styles.yes}>{yesVotes}</Text>
                        </View>
                        <View>
                          <Text style={styles.no}>No</Text>
                          <Text style={styles.no}>{noVotes}</Text>
                        </View>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-around",
                          marginTop: 10,
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: "#45D721",
                            height: 4,
                            width: `${percentGreen}%`,
                          }}
                        />
                        <View
                          style={{
                            backgroundColor: "#FF0000",
                            height: 4,
                            width: `${percentRed}%`,
                          }}
                        />
                      </View>
                      <Button
                        onPress={() =>
                          this.props.navigation.navigate("ProposalDetails", {
                            proposalIndex,
                            item,
                          })
                        }
                        style={styles.btn}
                      >
                        <Text style={styles.btnText}>View Proposal</Text>
                      </Button>
                    </View>
                  </View>
                );
              })}
            </ScrollView>
          </Tab>
          <Tab
            activeTextStyle={styles.activeTextStyle}
            activeTabStyle={styles.activeTab}
            tabStyle={styles.tab}
            textStyle={styles.tabText}
            heading="Aborted"
          >
            <View style={{ alignSelf: "flex-end", marginTop: 10 }}></View>
            <ScrollView>
              {proposalsAborted.map((item, index) => {
                const {
                  yesVotes,
                  noVotes,
                  sharesRequested,
                  tokenTribute,
                  proposalIndex,
                } = item;
                const tokensBN = new BN(tokenTribute, 10);
                const yesVotesN = parseInt(yesVotes);
                const noVotesN = parseInt(noVotes);
                const totalVotes = yesVotesN + noVotesN;
                let percentGreen = 50;
                let percentRed = 50;

                if (yesVotesN > 0 && noVotesN > 0) {
                  percentGreen = (yesVotesN * 100) / totalVotes;
                  percentRed = (noVotesN * 100) / totalVotes;
                } else if (yesVotesN > 0 && noVotesN === 0) {
                  percentGreen = 100;
                  percentRed = 0;
                } else if (yesVotesN === 0 && noVotesN > 0) {
                  percentGreen = 0;
                  percentRed = 100;
                }

                return (
                  <View
                    key={index}
                    style={[styles.container, { paddingTop: 0 }]}
                  >
                    <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                      <Text style={styles.header}>undefined</Text>
                      <Text style={styles.header}>undefined</Text>
                      <View style={styles.tabTable}>
                        <View>
                          <Text style={styles.subheader}>Tribute</Text>
                          <Text style={styles.value}>
                            Ξ {this.formatBalance(tokensBN)}
                          </Text>
                        </View>
                        <View>
                          <Text style={styles.subheader}>Shares Requested</Text>
                          <Text style={styles.value}>{sharesRequested}</Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: "row", marginTop: 40 }}>
                        <Image
                          style={styles.clockIcon}
                          source={Images.CLOCK}
                          resizeMode="contain"
                        ></Image>
                        <Text style={styles.days}>startingPeriod</Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          marginTop: 40,
                          justifyContent: "space-around",
                        }}
                      >
                        <View>
                          <Text style={styles.yes}>Yes</Text>
                          <Text style={styles.yes}>{yesVotes}</Text>
                        </View>
                        <View>
                          <Text style={styles.no}>No</Text>
                          <Text style={styles.no}>{noVotes}</Text>
                        </View>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-around",
                          marginTop: 10,
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: "#45D721",
                            height: 4,
                            width: `${percentGreen}%`,
                          }}
                        />
                        <View
                          style={{
                            backgroundColor: "#FF0000",
                            height: 4,
                            width: `${percentRed}%`,
                          }}
                        />
                      </View>
                      <Button
                        onPress={() =>
                          this.props.navigation.navigate("ProposalDetails", {
                            proposalIndex,
                            item,
                          })
                        }
                        style={styles.btn}
                      >
                        <Text style={styles.btnText}>View Proposal</Text>
                      </Button>
                    </View>
                  </View>
                );
              })}
            </ScrollView>
          </Tab>
        </Tabs>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    padding: 20,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0, 0, 0, 0.15)",
  },
  headerText: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#000000",
  },
  tab: {
    backgroundColor: "transparent",
  },
  activeTab: {
    backgroundColor: "transparent",
  },
  activeTextStyle: {
    color: "#000000",
  },
  tabText: {
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    textAlign: "center",
    color: "rgba(0, 0, 0, 0.35)",
  },
  plusIcon: {
    height: 42,
    width: 21,
    alignSelf: "flex-end",
    marginRight: 20,
  },
  header: {
    fontFamily: "Roboto-Bold",
    fontSize: 24,
    color: "#000000",
  },
  tabTable: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 25,
  },
  subheader: {
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    color: "#000000",
  },
  value: {
    fontFamily: "Roboto-Regular",
    fontSize: 30,
    color: "rgba(0, 0, 0, 0.82)",
  },
  clockIcon: {
    width: 20,
    height: 20,
    marginRight: 15,
  },
  days: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
  },
  yes: {
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    textAlign: "center",
    color: "#45D721",
  },
  no: {
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    textAlign: "center",
    color: "#FF0000",
  },
  greenLine: {
    backgroundColor: "#45D721",
    height: 4,
    width: "65%",
  },
  redLine: {
    backgroundColor: "#FF0000",
    height: 4,
    width: "35%",
  },
  btn: {
    backgroundColor: "#000000",
    borderRadius: 8,
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 25,
    marginTop: 25,
  },
  btnText: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    textAlign: "center",
    color: "#FFFFFF",
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProposalScreen);
