import React, { Component } from "react";
import {
  Clipboard,
  Image,
  View,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from "react-native";
import { Button, Icon, Thumbnail } from "native-base";
import DropDownItem from "react-native-drop-down-item";
import Share from "react-native-share";
import { connect } from "react-redux";
import Identicon from "identicon.js";
import { formatBalance } from "./Utils";
import Images from "../../../assets";

const mapStateToProps = state => ({ ...state.home, sdk: state.sdk });

const mapDispatchToProps = dispatch => ({});

class MemberDetails extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Member",
      headerStyle: {
        backgroundColor: "#fff",
        elevation: 0,
        textAlign: "center",
        borderBottomWidth: 1,
        borderColor: "#000",
      },
      headerTitleStyle: styles.headerTitle,
      headerRight: <View />,
    };
  };

  shareToSocial = address => () => {
    Share.open({
      message: address,
    })
      .then(res => {
        Alert.alert(
          "Successfully Shared",
          "You've successfully shared your address.",
          [{ text: "OK", onPress: () => {} }],
          { cancellable: false }
        );
      })
      .catch(err => {
        Alert.alert(
          "Error Occurred",
          "An error occurred while trying to share address.",
          [{ text: "OK", onPress: () => {} }],
          { cancellable: false }
        );
      });
  };

  copyToClipboard = address => () => {
    Clipboard.setString(address);
  };

  render() {
    const { account } = this.props.sdk;
    const accountName = account && account.name ? account.name : "undefined";
    const accountAddress =
      account && account.address ? account.address : "undefined";
    const accountAddressSpliced =
      accountAddress.slice(0, 7) +
      "..." +
      accountAddress.slice(-4, accountAddress.length);
    const accountRealBalance = formatBalance(
      account && account.balance.real ? account.balance.real : 0,
      2
    );
    const tribute = this.props.navigation.getParam("tribute", "0");
    const shares = this.props.navigation.getParam("shares", "0");
    const details = this.props.navigation.getParam("item", null);

    const avatar = new Identicon(accountAddress, 420).toString();
    const avatarUri = `data:image/png;base64,${avatar}`;
    const votes = details.votes || [1, 2];

    return (
      <ScrollView
        contentContainerStyle={{ flexGrow: 1, justifyContent: "flex-start" }}
      >
        <View style={styles.container}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("MemberDetails")}
            style={styles.section1}
          >
            <Thumbnail circle large source={{ uri: avatarUri }} />
            <View style={{ paddingLeft: 55 }}>
              <Text style={styles.name}>{accountName}</Text>
              <Text style={styles.details}>{accountAddressSpliced}</Text>
              <View
                style={[
                  { flexDirection: "row", justifyContent: "space-between" },
                ]}
              >
                <Button
                  transparent
                  small
                  onPress={this.copyToClipboard(accountAddress)}
                >
                  <Text style={styles.copy}>Copy</Text>
                </Button>
                <Button
                  transparent
                  small
                  onPress={this.shareToSocial(accountAddress)}
                >
                  <Text style={styles.copy}>Send</Text>
                </Button>
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.section2}>
            <View>
              <Text style={[styles.tabTitle]}>Tribute</Text>
              <Text style={styles.value}>Ξ {tribute}</Text>
            </View>
            <View>
              <Text style={styles.tabTitle}>Shares</Text>
              <Text style={styles.value}>{shares}</Text>
            </View>
            <View>
              <Text style={styles.tabTitle}>Balance</Text>
              <Text style={styles.value}>Ξ {accountRealBalance}</Text>
            </View>
          </View>
          <View style={styles.delegateKeyDiv}>
            <View>
              <Text style={styles.name}>Delegate Key</Text>
              <Text style={styles.details}>{accountAddressSpliced}</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text style={[styles.details, { marginRight: 10 }]}>Edit</Text>
              <Image
                style={styles.editIcon}
                source={Images.RIGHT}
                resizeMode="contain"
              />
            </View>
          </View>
          <View style={[styles.dropdownTitleDiv, { marginTop: 15 }]}>
            <Text style={styles.dropdownTitle}>WatchList</Text>
            <Image
              style={styles.editIcon}
              source={Images.RIGHT}
              resizeMode="contain"
            />
          </View>
          <View style={[styles.dropdownTitleDiv]}>
            <Text style={styles.dropdownTitle}>Proposals</Text>
            <Image
              style={styles.editIcon}
              source={Images.RIGHT}
              resizeMode="contain"
            />
          </View>
          <View style={[styles.dropdownTitleDiv, { marginBottom: 20 }]}>
            <Text style={styles.dropdownTitle}>Vote History</Text>
            <Image
              style={styles.editIcon}
              source={Images.DOWN}
              resizeMode="contain"
            />
          </View>
        </View>
        <View style={styles.line} />
        <>
          {votes.map((item, index) => {
            return (
              <View key={index} style={styles.listItem}>
                <View style={{ width: "85%" }}>
                  <Text style={[styles.desc, { marginBottom: 9 }]}>
                    2019-05-18
                  </Text>
                  <Text style={styles.headerText}>undefined</Text>
                  <Text style={styles.desc}>
                    Ξ 0 Tribute | 0 Shares | Processed
                  </Text>
                </View>
                <View
                  style={{
                    width: "15%",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Image
                    source={Images.STARSTRUCK}
                    style={styles.imageIcon}
                    resizeMode="contain"
                  />
                </View>
              </View>
            );
          })}
        </>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: 30,
    paddingRight: 30,
  },
  headerTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#000000",
  },
  starImage: {
    width: 30,
    height: 30,
  },
  thumbnail: {
    width: 100,
    height: 100,
  },
  section1: {
    paddingTop: 35,
    paddingBottom: 35,
    flexDirection: "row",
    alignItems: "center",
    // justifyContent: 'center'
  },
  name: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#000000",
  },
  details: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    paddingTop: 7,
    paddingBottom: 10,
  },
  copy: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
  },
  section2: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  tabTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    color: "#000000",
  },
  value: {
    fontFamily: "Roboto-Regular",
    fontSize: 30,
    color: "rgba(0, 0, 0, 0.82)",
  },
  dropDownItem: {
    // paddingTop: 5,
    // paddingBottom: 5,
    // backgroundColor: 'red',
    // height: 50
  },
  delegateKeyDiv: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 25,
  },
  editIcon: {
    width: 15,
    height: 22,
  },
  dropdownTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: 24,
    color: "#000",
  },
  dropdownTitleDiv: {
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },

  line: {
    backgroundColor: "rgba(0, 0, 0, 0.15)",
    height: 2,
    width: "100%",
  },
  listItem: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 20,
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderColor: "rgba(0, 0, 0, 0.15)",
    flexDirection: "row",
    alignItems: "center",
  },
  headerText: {
    fontFamily: "Roboto-Bold",
    fontSize: 21,
    color: "#000000",
  },
  imageIcon: {
    width: 48,
    height: 48,
  },
  desc: {
    fontFamily: "Roboto-Normal",
    fontSize: 16,
    color: "#000000",
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MemberDetails);
