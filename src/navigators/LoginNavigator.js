// @flow

import {createStackNavigator} from 'react-navigation';
import OnBoarding from '../components/containers/screens/Onboarding';
import SignUpEmailScreen from '../components/containers/screens/SignUpEmail';

import HomeScreen from '../components/containers/screens/Home';
import OnboardingScreen from '../components/containers/screens/Onboarding';
import OnboardingSignupScreen from '../components/containers/screens/Onboarding.Signup';
import OnboardingSigninLocalScreen from '../components/containers/screens/Onboarding.SignIn.Local';

const LoginNavigator = createStackNavigator(
  {
    OnBoarding: {
      screen: OnBoarding,
      navigationOptions: {
        header: null,
        title: 'OnBoarding',
      },
    },
    Signup: OnboardingSignupScreen,
    SigninLocal: OnboardingSigninLocalScreen,
  },
  {
    initialRouteName: 'OnBoarding',
  }
);

export default LoginNavigator;
