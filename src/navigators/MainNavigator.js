// @flow

import React, {Component} from 'react';
import {
  Image, Text, View
} from 'react-native';
import {createStackNavigator, createBottomTabNavigator, createAppContainer} from 'react-navigation';
import {FluidNavigator} from 'react-navigation-fluid-transitions';
import OnBoarding from '../components/containers/screens/Onboarding';
import Home from '../components/containers/screens/Home';
import Proposal from '../components/containers/screens/Proposal';
import AddProposal from '../components/containers/screens/Proposal.Add';
import ProposalSubmitted from '../components/containers/screens/Proposal.Submitted';
import ProposalDetails from '../components/containers/screens/Proposal.Details';
import Members from '../components/containers/screens/Members';
import MemberProfile from '../components/containers/screens/Member.Profile';
import MemberDetails from '../components/containers/screens/Member.Details';
import MemberSettings from '../components/containers/screens/Member.Settings';
import VideoFullScreen from '../components/containers/screens/VideoFullScreen';

import Images from '../assets';

const globalStackNaviationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        header: null
      }
    },
    MemberDetails: {
      screen: MemberDetails,
    },
    MemberSettings: {
      screen: MemberSettings
    }
  },
  {
    initialRouteName: 'Home',
  }
);

const ProposalStack = createStackNavigator(
  {
    Home: {
      screen: Proposal,
      navigationOptions: {
        header: null,
      },
    },
    AddProposal: {
      screen: AddProposal,
    },
    ProposalSubmitted: {
      screen: ProposalSubmitted
    },
    ProposalDetails: {
      screen: ProposalDetails,
    },
    VideoFullScreen: {
      screen: VideoFullScreen,
      navigationOptions: {
        header: null
      }
    },

  },
  {
    initialRouteName: 'Home',
  }
);


const MemberStack = createStackNavigator(
  {
    Home: {
      screen: Members,
      navigationOptions: {
        header: null,
      },
    },
    MemberProfile: {
      screen: MemberProfile
    },
  },
  {
    initialRouteName: 'Home',
  }
);

HomeStack.navigationOptions = globalStackNaviationOptions;
ProposalStack.navigationOptions = globalStackNaviationOptions;
MemberStack.navigationOptions = globalStackNaviationOptions;

export default createBottomTabNavigator(
  {
    ProposalTab: ProposalStack,
    HomeTab: HomeStack,
    MemberTab: MemberStack,
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarLabel: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let label = 'Home';
        if (routeName === 'ProposalTab') {
          label = 'Proposal';
        } else if (routeName === 'MemberTab') {
          label = 'Member';
        }

        return <Text style={{textAlign: 'center'}}>{label}</Text>;
      },
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let icon = Images.HOME;
        if (routeName === 'ProposalTab') {
          icon = Images.PROPOSAL;
        } else if (routeName === 'MemberTab') {
          icon = Images.MEMBER;
        }

        return <Image style={{height: 20, width: 20}} resizeMode="contain" source={icon} />;
      },
    }),
    tabBarOptions: {
      showIcon: true,
      showLabel: true,
      style: {
        height: 70,
        // padding: 30,
        // paddingLeft: 50,
        // paddingRight: 50,
        backgroundColor: '#fff',
      },
      tabStyle: {
        marginTop: 10,
        marginBottom: 10
        // backgroundColor: '#161616',
      },
      labelStyle: {
        fontFamily: 'Roboto-Bold',
        fontSize: 16,
        color: '#000000'
      }
    },
    initialRouteName: 'HomeTab',
  },
);
