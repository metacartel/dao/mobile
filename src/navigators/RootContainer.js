// @flow

import React from 'react';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import PropTypes from 'prop-types';

import MainNavigator from './MainNavigator';
import LoginNavigator from './LoginNavigator';

class RootContainer extends React.Component {
  constructor(props) {
    super(props);

    let goToNavigator;

    const {profile} = props;
    if (profile) {
      goToNavigator = 'MainNavigator';
    } else {
      goToNavigator = 'LoginNavigator';
    }

    this.state = {
      goToNavigator,
    };
  }

  render() {
    const {goToNavigator} = this.state;

    const Navigator = createAppContainer(makeRootNavigator(goToNavigator));
    return <Navigator />;
  }
}

const makeRootNavigator = goToNav => createSwitchNavigator(
  {
    LoginNavigator: {
      screen: LoginNavigator,
    },
    MainNavigator: {
      screen: MainNavigator,
    },
  },
  {
    initialRouteName: goToNav,
  },
);

export default RootContainer;
