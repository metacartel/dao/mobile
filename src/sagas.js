// @flow

import {takeLatest, call, fork, put, all} from 'redux-saga/effects';
import {NavigationActions} from 'react-navigation';
import {anyToBN} from '@netgum/utils';
import {sdk, estimateAccountDeployment, deployAccount} from './helpers/sdk';
import firebase from 'react-native-firebase';
import * as actionTypes from './constants/actionTypes';
import * as storage from './helpers/storage';
import * as socialAuth from './helpers/social.auth';
import * as localAuth from './helpers/local.auth';
import * as endpoint from './helpers/endpoint';
import * as coinbase from './helpers/coinbase';

function* bootGoogle() {
  try {
    yield call(socialAuth.bootGoogle);
  } catch (error) {
    console.error(error);
  }
}

function* loginWithGoogle() {
  try {
    const credential = yield call(socialAuth.loginWithGoogle);
    yield put({type: actionTypes.REGISTER_ARCHANOVA_DETAILS, payload: credential});
    yield put({type: actionTypes.CARTEL_CALL_SUCCESS, payload: credential});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* loginWithFacebook() {
  try {
    const credential = yield call(socialAuth.loginWithFacebook);
    yield put({type: actionTypes.REGISTER_ARCHANOVA_DETAILS, payload: credential});
    yield put({type: actionTypes.CARTEL_CALL_SUCCESS, payload: credential});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* loginWithTwitter() {
  try {
    const credential = yield call(socialAuth.loginWithTwitter);
    yield put({type: actionTypes.REGISTER_ARCHANOVA_DETAILS, payload: credential});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* signUp(params) {
  try {
    const {email, password} = params.payload;
    const credential = yield call(localAuth.createAccount, email, password);
    // yield call(credential.user.sendEmailVerification);
    yield put({type: actionTypes.REGISTER_ARCHANOVA_DETAILS, payload: credential});
    yield put({type: actionTypes.CARTEL_CALL_SUCCESS, payload: credential});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* signInLocal(params) {
  try {
    const {email, password} = params.payload;
    const credential = yield call(localAuth.signInLocal, email, password);
    yield put({type: actionTypes.REGISTER_ARCHANOVA_DETAILS, payload: credential});
    yield put({type: actionTypes.CARTEL_CALL_SUCCESS, payload: credential});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* signOut() {
  try {
    yield call(localAuth.signOut);
    yield put({type: actionTypes.CARTEL_CALL_SUCCESS});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* switchMainNavigator() {
  yield put(NavigationActions.navigate({routeName: 'MainNavigator'}));
}

function* switchLoginNavigator() {
  yield put(NavigationActions.navigate({routeName: 'LoginNavigator'}));
}

function* authStateChanged(params) {
  yield call(localAuth.authStateChanged, params.payload);
}

function* getListOfApplicants() {
  try {
    const data = yield call(endpoint.getListOfApplicants);
    yield put({type: actionTypes.CARTEL_CALL_LIST_SUCCESS, payload: data});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_LIST_FAILURE});
  }
}

function* getListOfMembers() {
  try {
    const data = yield call(endpoint.getListOfMembers);
    yield put({type: actionTypes.CARTEL_CALL_LIST_SUCCESS, payload: data});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_LIST_FAILURE});
  }
}

function* getListOfProposals() {
  try {
    const data = yield call(endpoint.getListOfProposals);
    yield put({type: actionTypes.CARTEL_CALL_LIST_SUCCESS, payload: data});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_LIST_FAILURE});
  }
}

function* getListOfVotes() {
  try {
    const data = yield call(endpoint.getListOfVotes);
    yield put({type: actionTypes.CARTEL_CALL_LIST_SUCCESS, payload: data});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_LIST_FAILURE});
  }
}

function* getExchangeRates() {
  try {
    const data = yield call(coinbase.getExchangeRates);
    const parseData = yield data.json();
    const usdRate = yield parseData.data.rates.USD;
    yield put({type: actionTypes.GET_EXCHANGE_RATES_SUCCESS, payload: {usdRate}});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* estimateAndDeployAccount() {
  try {
    const {account, initialized} = sdk.state;
    if (initialized) {
      const estimate = yield call(estimateAccountDeployment);
      const estimatedBN = estimate.totalCost;
      const balanceBN = account.balance.real;

      if (estimatedBN.lt(balanceBN) && account.state === "Created") {
        const hash = yield call(deployAccount);
      } else {
        throw new Error('Insufficient balance to deploy account');
      }
      yield put({type: actionTypes.CARTEL_CALL_SUCCESS});
    }
  } catch (error) {
    console.log(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* registerArchanovaDetails(params) {
  try {
    const currentUser = params.payload.user;
    const {accountAddress, deviceAddress} = sdk.state;
    yield call(storage.registerArchanovaDetails, currentUser, {accountAddress, deviceAddress});
    yield put({type: actionTypes.CARTEL_CALL_SUCCESS});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* getArchanovaDetails(params) {
  try {
    const currentUser = params.payload;
    yield call(storage.getArchanovaDetails, currentUser);
    yield put({type: actionTypes.CARTEL_CALL_SUCCESS});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* refreshHome() {
  try {
    yield put({type: actionTypes.CARTEL_CALL_SUCCESS});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* submitProposal(params) {
  try {
    yield put({type: actionTypes.CARTEL_CALL_SUCCESS});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* submitVote(params) {
  try {
    yield put({type: actionTypes.CARTEL_CALL_SUCCESS});
  } catch (error) {
    console.error(error);
    yield put({type: actionTypes.CARTEL_CALL_FAILURE});
  }
}

function* homeLoaded() {
  yield put({type: actionTypes.GET_EXCHANGE_RATES});
}

function* membersLoaded(params) {
  yield put({type: actionTypes.GET_LIST_OF_MEMBERS});
}

function* proposalsLoaded(params) {
  yield put({type: actionTypes.GET_LIST_OF_PROPOSALS});
}

function* rootSaga() {
  yield all([
    takeLatest(actionTypes.BOOT_GOOGLE, bootGoogle),
    takeLatest(actionTypes.LOGIN_WITH_GOOGLE, loginWithGoogle),
    takeLatest(actionTypes.LOGIN_WITH_FACEBOOK, loginWithFacebook),
    takeLatest(actionTypes.LOGIN_WITH_TWITTER, loginWithTwitter),
    takeLatest(actionTypes.SIGNUP, signUp),
    takeLatest(actionTypes.SIGNIN_LOCAL, signInLocal),
    takeLatest(actionTypes.SIGNOUT, signOut),
    takeLatest(actionTypes.SWITCH_MAIN_NAVIGATOR, switchMainNavigator),
    takeLatest(actionTypes.SWITCH_LOGIN_NAVIGATOR, switchLoginNavigator),
    takeLatest(actionTypes.AUTH_STATE_CHANGED, authStateChanged),
    takeLatest(actionTypes.GET_LIST_OF_APPLICANTS, getListOfApplicants),
    takeLatest(actionTypes.GET_LIST_OF_MEMBERS, getListOfMembers),
    takeLatest(actionTypes.GET_LIST_OF_PROPOSALS, getListOfProposals),
    takeLatest(actionTypes.GET_LIST_OF_VOTES, getListOfVotes),
    takeLatest(actionTypes.GET_EXCHANGE_RATES, getExchangeRates),
    takeLatest(actionTypes.DEPLOY_ACCOUNT, estimateAndDeployAccount),
    takeLatest(actionTypes.REGISTER_ARCHANOVA_DETAILS, registerArchanovaDetails),
    takeLatest(actionTypes.GET_ARCHANOVA_DETAILS, getArchanovaDetails),
    takeLatest(actionTypes.REFRESH_HOME, refreshHome),
    takeLatest(actionTypes.SUBMIT_PROPOSAL, submitProposal),
    takeLatest(actionTypes.SUBMIT_VOTE, submitVote),
    takeLatest(actionTypes.HOME_LOADED, homeLoaded),
    takeLatest(actionTypes.MEMBERS_LOADED, membersLoaded),
    takeLatest(actionTypes.PROPOSALS_LOADED, proposalsLoaded),
  ]);
}

export default rootSaga;
